﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using TheraBytes.BetterUi;
public class DragDropWindow : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	Vector2 initMousePos; //Initial click position
	Vector2 currentMousePos; //Current mouse position OnDrag
	Vector2 offsetClickPos; //Offset click position for the window to reposition itself instead of vector3.zero

	RectTransform _RectCanvas;
	RectTransform RectCanvas
	{
		get { return _RectCanvas = _RectCanvas ?? UICanvasController.Instance.GetComponent<RectTransform>(); }
	}

	RectTransform _virtualMouse;
	RectTransform VirtualMouse
	{
		get { return _virtualMouse = _virtualMouse ?? FindObjectOfType<MouseBehaviour>().mouse.GetComponent<RectTransform>(); }
	}

	WindowBehaviour _windowBehaviour;
	WindowBehaviour WindowBehaviour
	{
		get { return _windowBehaviour = _windowBehaviour ?? transform.parent.GetComponentInParent<WindowBehaviour>(); }
	}

	/// <summary>
	/// The Rect Transform of the parent attached to this transform
	/// </summary>
	RectTransform _RectParent;
	RectTransform RectParent
	{
		get { return _RectParent = _RectParent ?? transform.parent.GetComponent<RectTransform>(); }
	}

	BetterImage _backgroundImage;
	public BetterImage BackgroundImage
	{
		get { return _backgroundImage = _backgroundImage ?? GetComponent<BetterImage>(); }
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (WindowBehaviour.IsMaximized)
			return;
		initMousePos = VirtualMouse.anchoredPosition;
		offsetClickPos = RectParent.anchoredPosition - initMousePos;
		DesktopScreen.Instance.OnWindowOrderChanged?.Invoke(WindowBehaviour);
		LeanTween.moveLocalZ(WindowBehaviour.gameObject, -50, 0.2f);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		initMousePos = Vector2.zero;
		offsetClickPos = Vector3.zero;
		currentMousePos = Vector2.zero;
		LeanTween.moveLocalZ(WindowBehaviour.gameObject, 0, 0.2f);
	}
	private void Update()
	{
		if (initMousePos != Vector2.zero)
		{
			currentMousePos = VirtualMouse.anchoredPosition;
			RectParent.anchoredPosition = new Vector2(
				Mathf.Clamp(currentMousePos.x + offsetClickPos.x, -CanvasBorder().x, CanvasBorder().x),
				Mathf.Clamp(currentMousePos.y + offsetClickPos.y, -CanvasBorder().y - 40, CanvasBorder().y -40)
				);
		}

	}

	public Vector2 CanvasBorder()
	{
		return RectCanvas.sizeDelta / 2 - RectParent.sizeDelta / 2;
	}

}
