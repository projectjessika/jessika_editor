﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProjectSettings : MonoBehaviour
{
	[SerializeField]
	public GameObject documentPrefab;
	[SerializeField]
	public GameObject imagePrefab;
	[SerializeField]
	public GameObject videoPrefab;
}
