﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using I2.Loc;
public class TextWindow : WindowBehaviour
{
	public TextMeshProUGUI tmpText;
	FileButton fileButton;

	string GetText
	{
		get { return LocalizationManager.CurrentLanguageCode == "de" ? fileButton.fileData.textDe : fileButton.fileData.textEn; }
	}

	public override void SetWindowState(bool visible, bool minimized)
	{
		base.SetWindowState(visible, minimized);
		tmpText.text = GetText;
	}

	public override void SetMediaFile(FileButton fileButton)
	{
		this.fileButton = fileButton;
	}
}
