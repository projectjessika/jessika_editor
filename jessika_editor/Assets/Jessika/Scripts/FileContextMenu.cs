﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FileContextMenu : ContextWindow
{
	public static FileContextMenu Instance;
    public Button favButton;
    public Button messageButton;
    public Image favIcon;
    public TextMeshProUGUI removeText;
    public TextMeshProUGUI addText;
    private FileData fileData;

	bool IsFav
	{
		get { return DecrypterManager.Instance.IsFavFile(fileData.name); }
	}
	Color FavIconColor
	{
		get { return IsFav ? new Color(1, 0.8f, 0, 1f) : new Color(0, 0, 0, 0.2f); }
	}

	private void Awake()
	{
		Instance = this;
	}
	private void Start()
	{
		favButton.onClick.AddListener(delegate
		{
			DecrypterManager.Instance.AddRemoveFavFile(fileData);
			SetFavIcon();
		});

		messageButton.onClick.AddListener(delegate
		{
			ChatManager.Instance.TriggerConversation(fileData.conversation);
		});
	}

	public void SetWindow(FileData fileData)
	{
		this.fileData = fileData;
		SetFavIcon();
		messageButton.interactable = fileData.triggersChat;
		base.SetWindow();
	}

	void SetFavIcon()
	{
		favIcon.color = FavIconColor;
		addText.gameObject.SetActive(!IsFav);
		removeText.gameObject.SetActive(IsFav);
	}

}
