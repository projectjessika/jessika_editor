﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;
public class MouseBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public static MouseBehaviour Instance;
	public Image mouse;

	public Sprite pointerCursor;
	public Sprite hoverCursor;
	public Sprite inputCursor;
	public RadialLoader radialLoader;

	Canvas _canvas;
	Canvas canvas
	{
		get { return _canvas = _canvas ?? GetComponent<Canvas>(); }
	}

	PointerEventData m_PointerEventData;
	EventSystem m_EventSystem;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		Cursor.visible = false;
		m_EventSystem = GetComponent<EventSystem>();
	}

	private void Update()
	{
		if (UICanvasController.Instance.ActiveScreen == null)
			return;
		//Check if the left Mouse button is clicked
		Vector2 pos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out pos);
		mouse.transform.position = canvas.transform.TransformPoint(pos);
		if (mouseOnCanvas)
		{
			//Set up the new Pointer Event
			m_PointerEventData = new PointerEventData(m_EventSystem);
			//Set the Pointer Event Position to that of the mouse position
			m_PointerEventData.position = Input.mousePosition;

			//Create a list of Raycast Results
			List<RaycastResult> results = new List<RaycastResult>();
			results = results.OrderBy(x => x.distance).ToList();
			//Raycast using the Graphics Raycaster and mouse click position
			UICanvasController.Instance.ActiveScreen.GraphicRaycaster.Raycast(m_PointerEventData, results);
			if (results.Count == 0)
				return;
			if (results[0].gameObject.GetComponent<WindowButton>())
				SetMouseSprite(pointerCursor);
			else
			{
				//For every result returned, output the name of the GameObject on the Canvas hit by the Ray
				for (int i = 0; i < results.Count; i++)
				{
					if (results[i].gameObject.GetComponent<Button>() != null
						|| results[i].gameObject.GetComponent<Slider>() != null
						|| results[i].gameObject.GetComponent<DragDropWindow>() != null)
					{
						SetMouseSprite(hoverCursor);
						break;
					}
					else if (results[i].gameObject.GetComponent<InputField>() != null || results[i].gameObject.GetComponent<TMPro.TMP_InputField>())
					{
						SetMouseSprite(inputCursor);
						break;
					}
					else
						SetMouseSprite(pointerCursor);
				}
			}
		}
	}

	public void SetRadialLoader(bool visible)
	{
		radialLoader.gameObject.SetActive(visible);
		mouse.enabled = !visible;
	}

	public void SetMouseSprite(Sprite sprite)
	{
		if (mouse.sprite == sprite)
			return;
		else
			mouse.sprite = sprite;
		mouse.preserveAspect = false;
		mouse.preserveAspect = true;
	}

	public bool mouseOnCanvas;
	public void OnPointerEnter(PointerEventData eventData)
	{
		mouseOnCanvas = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		mouseOnCanvas = false;
	}
}
