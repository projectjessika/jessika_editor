﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskbarButton : MonoBehaviour
{

	public WindowBehaviour window;

	Button _Button;
	public Button Button
	{
		get { return _Button = _Button ?? GetComponent<Button>(); }
	}

	public Image icon;

	private void Start()
	{
		Button.onClick.AddListener(delegate
		{
			window.SetWindowState(true, false);
		});
	}

	public void SetTaskbarButton(WindowBehaviour window, Sprite sprite)
	{
		this.window = window;
		SetIcon(sprite);
	}

	public void SetIcon(Sprite sprite)
	{
		icon.sprite = sprite;
	}

	private void Update()
	{
		HighLightIcon();
	}

	void HighLightIcon()
	{
		if (window)
			if (!window.IsSelectedWindow || !window.Visible)
				Button.interactable = true;
			else
				Button.interactable = false;
		else
			Destroy(gameObject);
	}
}
