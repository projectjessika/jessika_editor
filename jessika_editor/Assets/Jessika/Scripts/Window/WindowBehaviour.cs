﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
using DG.Tweening;
using TheraBytes.BetterUi;
using System;

[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(WindowButton))]
public class WindowBehaviour : MonoBehaviour
{
	public TaskbarButton taskbarButton;
	public TextMeshProUGUI windowTitle;
	public GameObject shadow;
	public float tweenTime { get; set; } = 0.3f;
	public Action OnWindowResize;

	public bool IsMaximized { get; private set; } = false;

	private Vector2 windowSize;

	public bool IsTopWindow
	{
		get { return transform.GetSiblingIndex() == transform.parent.childCount - 1; }
	}

	RectTransform _rectTransform;
	public RectTransform RectTransform
	{
		get { return _rectTransform = _rectTransform ?? GetComponent<RectTransform>(); }
	}

	CanvasGroup _canvasGroup;
	CanvasGroup canvasGroup
	{
		get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); }
	}

	BetterImage _backgroundImage;
	BetterImage BackgroundImage
	{
		get { return _backgroundImage = _backgroundImage ?? GetComponent<BetterImage>(); }
	}

	DragDropWindow _dragDropWindow;
	DragDropWindow DragDropWindow
	{
		get { return _dragDropWindow = _dragDropWindow ?? GetComponentInChildren<DragDropWindow>(); }
	}

	public bool Visible
	{
		get { return canvasGroup.alpha == 1; }
	}

	public virtual void OnEnable()
	{
		if (DesktopScreen.Instance)
			DesktopScreen.Instance.OnWindowOrderChanged += OnWindowOrderChanged;
	}

	public virtual void OnDisable()
	{
		if (DesktopScreen.Instance)
			DesktopScreen.Instance.OnWindowOrderChanged -= OnWindowOrderChanged;
	}

	public virtual void Start()
	{
		windowSize = RectTransform.rect.size;
	}


	public virtual void OnWindowOrderChanged(WindowBehaviour window)
	{
		if (this == window)
			transform.SetAsLastSibling();
	}

	public void SetupWindow(string title, TaskbarButton taskbarButton)
	{
		windowTitle.text = title;
		this.taskbarButton = taskbarButton;
	}

	public void StretchAnchors(RectTransform rt)
	{
		rt.anchorMin = new Vector2(0, 0);
		rt.anchorMax = new Vector2(1, 1);
		rt.offsetMin = Vector2.zero;
		rt.offsetMax = Vector2.zero;
	}


	public virtual void SetWindowState(bool visible, bool minimized)
	{
		if (visible)
			DesktopScreen.Instance.OnWindowOrderChanged?.Invoke(this);
		canvasGroup.DOFade(visible ? 1 : 0, 0.2f).OnComplete(() =>
		{
			if (!minimized && !visible)
			{
				transform.localPosition = Vector2.zero;
				if (IsMaximized)
					SetWindowSize();
				transform.SetAsFirstSibling();
			}
			canvasGroup.interactable = visible;
			canvasGroup.blocksRaycasts = visible;
		});
		if (taskbarButton)
		{
			if (visible || minimized)
				SetTaskButton(true);
			else
				SetTaskButton(false);
		}
	}

	public bool IsSelectedWindow
	{
		get { return gameObject.activeSelf && transform.GetSiblingIndex() == transform.parent.childCount - 1; }
	}

	public void SetTaskButton(bool val)
	{
		if (!taskbarButton.gameObject.activeSelf)
			taskbarButton.transform.SetAsLastSibling();
		taskbarButton.gameObject.SetActive(val);
	}

	public virtual void SetWindowSize()
	{
		IsMaximized = !IsMaximized;
		OnWindowResize?.Invoke();
		//RectTransform.localPosition = Vector2.zero;
		RectTransform.DOKill();
		RectTransform.DOAnchorMin(IsMaximized ? Vector2.zero : Vector2.one * 0.5f, tweenTime);
		RectTransform.DOAnchorMax(IsMaximized ? Vector2.one : Vector2.one * 0.5f, tweenTime);
		RectTransform.DOAnchorPos(Vector2.zero, tweenTime);

		if (IsMaximized)
		{
			DOTween.To(() => RectTransform.offsetMin, x => RectTransform.offsetMin = x, new Vector2(0, 0), tweenTime);
			DOTween.To(() => RectTransform.offsetMax, x => RectTransform.offsetMax = x, new Vector2(0, 0), tweenTime);
		}
		else
			RectTransform.DOSizeDelta(windowSize, tweenTime);

		Vector2 backgroundBorder = IsMaximized ? Vector2.zero : new Vector2(15, 15);
		BackgroundImage.SpriteBorderScale.MinSize = backgroundBorder;
		DragDropWindow.BackgroundImage.SpriteBorderScale.MinSize = backgroundBorder;
		shadow.SetActive(!IsMaximized);
	}

	public virtual void SetMediaFile(FileButton fileButton) { }
	public virtual void SetPicture(Sprite sprite) { }

}
