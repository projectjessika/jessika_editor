﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
public class WindowButton : Button
{
	Button[] buttonsInChildren;

	WindowBehaviour _windowBehaviour;
	WindowBehaviour WindowBehaviour
	{
		get { return _windowBehaviour = _windowBehaviour ?? GetComponent<WindowBehaviour>(); }
	}

	protected override void Start()
	{
		base.Start();
		SetListeners();
	}


	void SetListeners()
	{
		buttonsInChildren = GetComponentsInChildren<Button>();

		foreach (Button button in buttonsInChildren)
			button.onClick.AddListener(SetTopWindow);
		if (DesktopScreen.Instance)
			DesktopScreen.Instance.OnWindowOrderChanged += SetInteractable;
	}

	void SetTopWindow()
	{
		if (WindowBehaviour.IsTopWindow)
			return;
		DesktopScreen.Instance.OnWindowOrderChanged?.Invoke(WindowBehaviour);
	}

	void SetInteractable(WindowBehaviour window)
	{
		interactable = window != WindowBehaviour;
	}

}
