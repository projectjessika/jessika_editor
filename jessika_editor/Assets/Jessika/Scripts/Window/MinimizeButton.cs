﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MinimizeButton : Button
{
    WindowBehaviour _windowBehaviour;
    WindowBehaviour windowBehaviour
    {
        get { return _windowBehaviour = _windowBehaviour ?? GetComponentInParent<WindowBehaviour>(); }
    }

    protected override void Start()
    {
        onClick.AddListener(Minimize);
    }
    void Minimize()
    {
        windowBehaviour.SetWindowState(false, true);
    }

    void SetInteraction()
    {
        interactable = !windowBehaviour.IsMaximized;
    }
}
