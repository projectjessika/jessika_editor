﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CloseButton : Button
{
    WindowBehaviour _windowBehaviour;
    WindowBehaviour windowBehaviour
    {
        get { return _windowBehaviour = _windowBehaviour ?? GetComponentInParent<WindowBehaviour>(); }
    }
    protected override void Start()
    {
        onClick.AddListener(Close);
    }
    void Close()
    {
        windowBehaviour.SetWindowState(false, false);
    }
    void SetInteraction()
    {
        interactable = !windowBehaviour.IsMaximized;
    }
}
