﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UniRx.Async;

[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(GraphicRaycaster))]
public class ScreenCanvas : MonoBehaviour
{
	GraphicRaycaster _graphicRaycaster;
	public GraphicRaycaster GraphicRaycaster
	{
		get { return _graphicRaycaster = _graphicRaycaster ?? GetComponent<GraphicRaycaster>(); }
	}

	CanvasGroup _canvasGroup;
	public CanvasGroup CanvasGroup
	{
		get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); }
	}

	private void OnEnable()
	{
		CanvasGroup.alpha = 0;
		Fade(1f);
	}

	public async UniTask Fade(float alpha)
	{
		CanvasGroup.DOFade(alpha, 0.5f);
		await UniTask.WaitUntil(() => CanvasGroup.alpha == alpha);
	}
}
