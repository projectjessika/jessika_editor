﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAnimation : MonoBehaviour
{

	CanvasGroup _canvasGroup;
	CanvasGroup canvasGroup
	{
		get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); }
	}

	float time = 10f;
	float Alpha { get { return canvasGroup.alpha == 0 ? 1 : 0; } }
	Vector3 NewScale { get { return transform.localScale == Vector3.one ? Vector3.zero : Vector3.one; } }

    void Start()
    {
		AnimateDots();
    }

	void AnimateDots()
	{
		LeanTween.scale(gameObject, NewScale, time);
		LeanTween.value(gameObject,canvasGroup.alpha, Alpha, time).setOnUpdate((float val) =>
		{
			canvasGroup.alpha = val;
		}).setOnComplete(AnimateDots);
	}
}
