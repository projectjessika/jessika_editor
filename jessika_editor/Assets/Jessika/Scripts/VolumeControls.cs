﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class VolumeControls : WindowBehaviour
{

	public AudioMixer audioMixer;
	public Slider noiseSlider;
	public Slider mediaSlider;

	// Use this for initialization
	public override void Start()
	{
		base.Start();
		mediaSlider.onValueChanged.AddListener(delegate
		{
			SetVideoGroup(mediaSlider.value);
		});

		noiseSlider.onValueChanged.AddListener(delegate
		{
			SetAmbientGroup(noiseSlider.value);
		});

	}

	public override void SetWindowState(bool visible, bool _minimized)
	{
		float val = visible ? 1 : 0;
		if (visible)
		{
			transform.localScale = Vector3.zero;
			gameObject.SetActive(true);
			LeanTween.scale(gameObject, new Vector3(val, val, val), .2f);
		}
		else
			LeanTween.scale(gameObject, new Vector3(val, val, val), .2f).setOnComplete(DisableWindow);
	}

	void DisableWindow()
	{
		gameObject.SetActive(false);
	}


	public void SetVideoGroup(float volume)
	{
		audioMixer.SetFloat("videoGroup", volume);
	}

	public void SetAmbientGroup(float volume)
	{
		audioMixer.SetFloat("ambientGroup", volume);
	}
}
