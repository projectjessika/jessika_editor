﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreateUser : MonoBehaviour
{


	// Use this for initialization
	void Start()
	{
		GetComponent<Button>().onClick.AddListener(delegate
		{
			SaveLoadManager.Instance.SetSelectedSaveGame(null);
			SceneManager.LoadScene(1, LoadSceneMode.Single);
		});
	}

}
