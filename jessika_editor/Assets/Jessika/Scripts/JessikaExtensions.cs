﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public static class JessikaExtensions 
{
    public static void CopyToClipboard(this string s)
    {
        TextEditor te = new TextEditor();
        te.text = s;
        te.SelectAll();
        te.Copy();
    }

    public static string ReplaceKeywords(this string text)
    {
        text = text.Replace("[PLAYERNAME]", DesktopScreen.Instance.playerName);
        text = text.Replace("[IP]", DesktopScreen.Instance.IP);
        text = text.Replace("[USERNAME]", DesktopScreen.Instance.userName);
        text = text.Replace("[PW]", DesktopScreen.Instance.password);
        return text;
    }

}
