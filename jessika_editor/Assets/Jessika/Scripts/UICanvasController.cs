﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;

public class UICanvasController : MonoBehaviour
{
	public static UICanvasController Instance;
	public GameObject radialLoader;
	public ScreenCanvas menuScreenPrefab, loginScreenPrefab, desktopScreenPrefab;
	public ScreenCanvas ActiveScreen { get; private set; }

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		ShowMenuScreen();
	}

	public  void ShowMenuScreen()
	{
		ShowScreen(menuScreenPrefab);
	}

	public void ShowLoginScreen()
	{
		SaveLoadManager.Instance.selectedSaveGame = null;
		ShowScreen(loginScreenPrefab);
	}

	public async void ShowDesktopScreen(string playerName)
	{
		ShowScreen(desktopScreenPrefab);
		await UniTask.WaitUntil(() => DesktopScreen.Instance != null);
		DesktopScreen.Instance.playerName = playerName;
		SaveLoadManager.Instance.Save();
	}

	private async void ShowScreen(ScreenCanvas screenCanvas)
	{
		if (ActiveScreen)
		{
			await ActiveScreen.Fade(0);
			Destroy(ActiveScreen.gameObject);
		}
		radialLoader.SetActive(true);
		await UniTask.Delay(TimeSpan.FromSeconds(1));
		ActiveScreen = Instantiate(screenCanvas, transform);
		ActiveScreen.transform.SetSiblingIndex(1);
		await UniTask.WaitUntil(() => ActiveScreen != null);
		radialLoader.SetActive(false);
	}
}
