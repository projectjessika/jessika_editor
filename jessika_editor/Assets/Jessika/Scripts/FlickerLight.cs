﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VLB;
using DG.Tweening;
public class FlickerLight : MonoBehaviour {

	VolumetricLightBeam _Light;
	VolumetricLightBeam Light
	{
		get { return _Light = _Light ?? GetComponent<VolumetricLightBeam>(); }
	}

	void Start()
	{
		FlickerFresnel();
	}


	void FlickerFresnel()
	{
		LeanTween.value(gameObject, Light.attenuationCustomBlending, Random.Range(0f, 0.8f), Random.Range(1f, 5f)).setOnUpdate((float val) =>
		{
			//Light.spotAngle = val;
			Light.attenuationCustomBlending = val;
			Light.UpdateAfterManualPropertyChange();
		}).setOnComplete(FlickerFresnel);
	}
}
