﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
	public static Settings Instance;
	public Action OnSystemLanguageChange;
	public Action OnAudioChange;
	public Action OnSubtitleChange;

	public string SystemLanguage { get { return LocalizationManager.CurrentLanguageCode; } }

	private string _subtitleLanguage;
	public string SubtitleLanguage
	{
		get { return string.IsNullOrEmpty(_subtitleLanguage) ? _subtitleLanguage = SystemLanguage : _subtitleLanguage; }
		set
		{
			_subtitleLanguage = value;
			OnSubtitleChange?.Invoke();
		}
	}

	private string _audioLanguage;
	public string AudioLanguage
	{
		get { return string.IsNullOrEmpty(_audioLanguage) ? _audioLanguage = SystemLanguage : _audioLanguage; }
		set
		{
			_audioLanguage = value;
			OnAudioChange?.Invoke();
		}
	}

	private void Awake()
	{
		Instance = this;
	}

	public void SetSystemLanguage()
	{
		LocalizationManager.CurrentLanguageCode = LocalizationManager.CurrentLanguageCode == "de" ? "en" : "de";
		OnSystemLanguageChange?.Invoke();
	}

	public void LoadLanguages(SaveLoadManager.SaveGame saveGame)
	{
		LocalizationManager.CurrentLanguageCode = saveGame.data.systemLanguage;
		SubtitleLanguage = saveGame.data.subtitleLanguage;
		AudioLanguage = saveGame.data.videoLanguage;
	}

}
