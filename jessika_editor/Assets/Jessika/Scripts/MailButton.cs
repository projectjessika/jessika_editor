﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class MailButton : MonoBehaviour
{
	//Fields of button itself
	public TextMeshProUGUI senderText;
	public TextMeshProUGUI subjectText;
	public TextMeshProUGUI dateText;
	public GameObject newIcon;
	public GameObject readIcon;
	public MailManager.MailSaveData mailSaveData;
	public AudioClip notificationSound;

	Button _button;
	public Button Button
	{
		get { return _button = _button ?? GetComponent<Button>(); }
	}

	MailData.Mail mail;

	private string date;
	private string time;
	private string content;

	CanvasGroup _canvasGroup;
	CanvasGroup CanvasGroup
	{ get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); } }

	private void OnEnable()
	{
		CanvasGroup.alpha = 0;
		CanvasGroup.DOFade(1, 0.2f);
	}


	/// <summary>
	/// Set the date of the button on instantiation
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="subject"></param>
	/// <param name="date"></param>
	/// <param name="content"></param>
	private void SetButton()
	{
		mail = MailManager.instance.GetMail(mailSaveData);
		string correspondentent = GetPreviewString(mail.correspondent);
		senderText.text = GetPreviewString(mail.correspondent);
		subjectText.text = GetPreviewString(mail.GetSubject);
		this.content = mail.contents[0].GetText;
		this.time = mailSaveData.dateTimeReceived.ToShortTimeString();
		this.date = mailSaveData.dateTimeReceived.ToShortDateString();
		dateText.text = time + "\n" + date;
		if (mailSaveData.opened)
			UpdateReadStatus();
	}

	string GetPreviewString(string text)
	{
		char[] characters = text.ToCharArray();
		string newText = null;
		if (characters.Length > 13)
		{
			for (int i = 0; i < 13; i++)
			{
				newText += characters[i].ToString();
				if (i == 12)
					newText += "...";
			}
		}
		else
			newText = text;
		return newText;
	}

	public void UpdateReadStatus()
	{
		newIcon.SetActive(false);
		readIcon.SetActive(true);
		if (!mailSaveData.opened)
			UpdateSaveData();
	}

	void UpdateSaveData()
	{
		for (int i = 0; i < MailManager.instance.mailSaveDatas.Count; i++)
		{
			if (mailSaveData == MailManager.instance.mailSaveDatas[i])
			{
				MailManager.instance.mailSaveDatas[i].opened = true;
				mailSaveData.opened = true;
				MailManager.instance.UpdateCount();
				Debug.Log(i + " updated");
			}
		}
	}

	private void Start()
	{
		SetButton();
		Button.onClick.AddListener(delegate
		{
			MailManager.instance.SetOpenMail(mail.correspondent, mail.GetSubject, time + "\n" + date, content);
			if (!mailSaveData.opened)
				UpdateReadStatus();
			MailManager.instance.MailButtonClicked(this);
		});
	}
}
