﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ChatData", menuName = "New ChatData")]
public class ChatData : ScriptableObject
{
	public enum ContactNames { Chad, Alan, Chem, Göring }
	[System.Serializable]
	public class Chat
	{
		public string chatName;
		public ContactNames[] contacts;

		public Sprite profilePicture;
		public List<ConversationData> conversations;
		public bool muted;
		public ChatWindow chatWindow { get; set; }
		public ChatButton chatButton { get; set; }
		public bool IsChatting { get; set; }

		public void SetChatElements(ChatWindow chatWindow, ChatButton chatButton)
		{
			this.chatWindow = chatWindow;
			chatWindow.SetWindow(this);
			this.chatButton = chatButton;
			chatButton.SetButton(this);
		}
	}
	public List<Chat> chats;

	[System.Serializable]
	public class ChatHistory
	{
		public string Name;
		public ConversationData.History[] histories;
		public Queue<string> queuedConversationNames;

		public ChatHistory(string name)
		{
			Name = name;
		}
	}
}
