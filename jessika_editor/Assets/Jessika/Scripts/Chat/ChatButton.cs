﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class ChatButton : MonoBehaviour
{
	public Button button;
	public Image profilePicureImage;
	public TextMeshProUGUI contactName;
	public Image NotificationImage;
	public GameObject muteIcon;
	public ChatData.Chat chat;

	public void SetButton(ChatData.Chat chat)
	{
		this.chat = chat;
		profilePicureImage.sprite = chat.profilePicture;
		contactName.text = chat.chatName;
		muteIcon.SetActive(chat.muted);
		button.onClick.AddListener(delegate
		{
			ChatManager.Instance.ShowWindow(chat);
			ChatManager.Instance.desktopButton.SetNotification(false);
			if (!chat.IsChatting)
				SetNotification(false);
		});
	}

	public void SetNotification(bool val)
	{
		contactName.fontStyle = val ? FontStyles.Bold : FontStyles.Normal;
		NotificationImage.gameObject.SetActive(val);
	}
}
