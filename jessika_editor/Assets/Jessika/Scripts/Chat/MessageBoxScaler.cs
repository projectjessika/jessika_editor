﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MessageBoxScaler : MonoBehaviour
{
	public enum MessageType { PLAYER, SPEAKER, DATE }
	public MessageType messageType;

	public TextMeshProUGUI contact;
	public TextMeshProUGUI message;
	public TextMeshProUGUI time;
	public RectTransform messageText;
	public RectTransform messageBox;
	public Button messageBoxButton;

	string[] texts;
	string GetText
	{
		get { return texts.Length == 1 ? texts[0] : (Settings.Instance.SystemLanguage == "de" ? texts[0] : texts[1]); }
	}

	private void Start()
	{
		Settings.Instance.OnSystemLanguageChange += UpdateMessage;
	}

	void UpdateMessage()
	{
		message.text = GetText.ReplaceKeywords();
	}


	public void SetMessage(string[] texts, string time, string contactName, Color nameColor, string stringToCopy = null)
	{
		messageBox.localScale = Vector3.zero;
		if (message)
		{
			this.texts = texts;
			UpdateMessage();
		}
		if (this.time)
			this.time.text = time;
		LeanTween.scale(messageBox, Vector3.one, 0.3f);
		if (contact != null)
		{
			contact.text = contactName;
			contact.gameObject.SetActive(contactName != null);
			contact.color = nameColor;
		}
		if (messageBoxButton)
			messageBoxButton.interactable = stringToCopy != null;
		if (stringToCopy != null)
		{
			messageBoxButton.onClick.AddListener(delegate
			{
				Debug.Log(stringToCopy);
				CopyPasteContextMenu.Instance.SetWindow(stringToCopy);
			});
		}
	}
}
