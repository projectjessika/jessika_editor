﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
[CustomEditor(typeof(ConversationData))]
public class ConversationDataEditor : Editor
{
	public SerializedProperty trigger;
	public SerializedProperty responseNodes;
	public SerializedProperty contactNames;

	ConversationData conversationData;

	void OnEnable()
	{
		trigger = serializedObject.FindProperty("trigger");
		responseNodes = serializedObject.FindProperty("responseNodes");
		contactNames = serializedObject.FindProperty("contactNames");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		ArrayGUI();
		serializedObject.ApplyModifiedProperties();
	}

	private void ArrayGUI()
	{
		conversationData = target as ConversationData;
		var buttonStyle = new GUIStyle(GUI.skin.button);
		buttonStyle.normal.textColor = Color.red;
		if (GUILayout.Button("Save"))
		{
			EditorUtility.SetDirty(target);
			AssetDatabase.SaveAssets();
		}
		EditorGUILayout.Space();
		EditorGUILayout.PropertyField(trigger, new GUIContent("Trigger"));
		if (conversationData.trigger != ConversationData.Trigger.History)
			conversationData.triggerRequirementAmount = EditorGUILayout.IntSlider("Trigger file amount", conversationData.triggerRequirementAmount, 0, 20);
		conversationData.isGroupChat = EditorGUILayout.Toggle("Group Chat", conversationData.isGroupChat);


		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Response"))
		{
			responseNodes.arraySize++;
		}
		GUILayout.EndHorizontal();

		if (conversationData.responseNodes == null)
			return;
		for (int i = 0; i < conversationData.responseNodes.Count; i++)
		{
			GUILayout.BeginVertical("GroupBox");
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(responseNodes.GetArrayElementAtIndex(i), new GUIContent("Response " + (i).ToString() + ": "), false);
			SerializedProperty responseNode = responseNodes.GetArrayElementAtIndex(i);


			if (responseNode.isExpanded)
				if (GUILayout.Button("Remove", buttonStyle))
				{
					responseNodes.DeleteArrayElementAtIndex(i);
					break;
				}
			GUILayout.EndHorizontal();
			if (responseNode.isExpanded)
			{
				EditorGUILayout.Space();
				DisplayResponseNode(conversationData.responseNodes[i], responseNode);
			}
			GUILayout.EndHorizontal();
		}



	}

	void DisplayResponseNode(ConversationData.ResponseNode responseNode, SerializedProperty prop)
	{
		if (conversationData.isGroupChat)
			EditorGUILayout.PropertyField(prop.FindPropertyRelative("contact"), new GUIContent("Contact"));
		//responseNode.name = EditorGUILayout.TextField("Contact Name", responseNode.name);
		responseNode.en = EditorGUILayout.TextField("EN", responseNode.en);
		responseNode.de = EditorGUILayout.TextField("DE", responseNode.de);
		responseNode.typeTime = EditorGUILayout.Slider("Response time", responseNode.typeTime, 0f, 5f);
		responseNode.canAnswer = EditorGUILayout.Toggle("Can Answer", responseNode.canAnswer);
		if (responseNode.canAnswer)
		{
			EditorGUILayout.PropertyField(prop.FindPropertyRelative("options"), new GUIContent("Answers"), true);
			responseNode.endsConversation = false;
		}
		else
		{
			responseNode.options = null;
			responseNode.endsConversation = EditorGUILayout.Toggle("Ends Conversation", responseNode.endsConversation);
		}
	}


	public void DrawUILine(Color color, int thickness = 2, int padding = 10)
	{
		Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
		r.height = thickness;
		r.y += padding / 2;
		r.x -= 2;
		r.width += 6;
		EditorGUI.DrawRect(r, color);
	}


}
#endif

