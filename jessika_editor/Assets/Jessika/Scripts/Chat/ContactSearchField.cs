﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactSearchField : TMPro.TMP_InputField
{
	protected override void Start()
	{
		onValueChanged.AddListener(delegate
		{
			foreach (ChatButton contactButton in ChatManager.Instance.contactButtons)
			{
				if (contactButton.contactName.text.ToLower() == text.ToLower() || string.IsNullOrEmpty(text) && contactButton.chat.chatWindow.content.transform.childCount > 2)
					contactButton.gameObject.SetActive(true);
				else
					contactButton.gameObject.SetActive(false);

			}
		});
	}
}
