﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
public class ChatAudioFile : MonoBehaviour
{

	public AudioClip audioClip;
	public Button playButton;
	public Button pauseButton;
	public Slider slider;
	public TextMeshProUGUI audioTime;
	public TextMeshProUGUI sentTime;

	AudioSource _AudioSource;
	public AudioSource AudioSource
	{
		get { return _AudioSource = _AudioSource ?? GetComponent<AudioSource>(); }
	}

	//Transforms a time into a readable string eg. 05:45
	string TimeDisplay(float time)
	{
		return string.Format("{0:00}:{1:00}", Mathf.Floor(time / 60), time % 60);
	}

	float CurrentTimeDisplay
	{
		get { return slider.value * (float)audioClip.length; }
	}

	public void SetAudioMessage(AudioClip clip, string time)
	{
		audioClip = clip;
		AudioSource.clip = clip;
		sentTime.text = time;
	}
	private void Update()
	{
		playButton.gameObject.SetActive(!AudioSource.isPlaying);
		pauseButton.gameObject.SetActive(!playButton.gameObject.activeSelf);
	}

	private void Start()
	{
		playButton.onClick.AddListener(delegate
		{
			AudioSource.Play();
		});

		pauseButton.onClick.AddListener(delegate
		{
			AudioSource.Pause();
		});
	}

	public void SetTime(float time)
	{
		AudioSource.time = time;
		AudioSource.Play();
	}


}
