﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine.UI;
using I2.Loc;
using UniRx.Async;

public class ChatManager : WindowBehaviour
{
	public static ChatManager Instance;
	public ChatData chatData;
	public ChatData.ChatHistory[] chatHistories;
	public DesktopButton desktopButton;
	public Color buttonColor;
	//Assign prefabs
	public GameObject chatWindowPrefab;
	public GameObject chatWindowButtonPrefab;
	//Assign Components of ChatSystem
	public GameObject ChatTabButtons;
	public GameObject conversationContainer;
	public GameObject dateTextPrefab;
	public GameObject leftMessagePrefab;
	public GameObject rightMessagePrefab;
	public GameObject audioMessagePrefab;

	public List<AudioClip> audioMessages;

	private List<ChatWindow> chatwindows = new List<ChatWindow>();
	public List<ChatButton> contactButtons { get; private set; } = new List<ChatButton>();

	AudioSource _Audio;
	public AudioSource Audio
	{
		get { return _Audio = _Audio ?? GetComponent<AudioSource>(); }
	}

	Color[] _nameColors;
	public Color[] NameColors
	{
		get
		{
			if (_nameColors == null)
			{
				_nameColors = new Color[Enum.GetValues(typeof(ChatData.ContactNames)).Length];
				for (int i = 0; i < _nameColors.Length; i++)
				{
					_nameColors[i] = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
					Debug.Log(_nameColors[i]);
				}
			}
			return _nameColors;
		}
	}


	[Serializable]
	public class Sounds
	{
		public AudioClip notification;
		public AudioClip typing;
		public AudioClip sent;
		public AudioClip received;
	}
	public Sounds chatSounds;

	private void Awake()
	{
		Instance = this;
		if (SaveLoadManager.Instance && SaveLoadManager.Instance.SaveGameSelected)
			chatHistories = SaveLoadManager.Instance.selectedSaveGame.data.chatHistories;
	}

	public override void Start()
	{
		base.Start();
		StartChat();
	}

	async void StartChat()
	{
		await UniTask.Delay(TimeSpan.FromSeconds(0.5f));
		await UniTask.WaitUntil(() => SaveLoadManager.Instance != null);
		InstantiateChatWindows();
	}
	public void InstantiateChatWindows()
	{
		for (int i = 0; i < chatData.chats.Count; i++)
		{
			ChatWindow chatWindow = Instantiate(chatWindowPrefab, conversationContainer.transform).GetComponent<ChatWindow>();
			ChatButton contactButton = Instantiate(chatWindowButtonPrefab, ChatTabButtons.transform).GetComponent<ChatButton>();
			chatData.chats[i].SetChatElements(chatWindow, contactButton);
			chatwindows.Add(chatWindow);
			contactButtons.Add(contactButton);

			contactButton.gameObject.SetActive(chatWindow.content.transform.childCount > 1);
		}

		if (chatHistories.Length == 0)
		{
			chatHistories = new ChatData.ChatHistory[chatData.chats.Count];
			for (int i = 0; i < chatHistories.Length; i++)
				chatHistories[i] = new ChatData.ChatHistory(chatData.chats[i].chatName);
		}

		InstantiateConversations();

	}


	string GetContactName(ConversationData conversation, ConversationData.ResponseNode responseNode)
	{
		return conversation.isGroupChat ? responseNode.contact.ToString() : null;
	}

	public void InstantiateConversations()
	{
		for (int i = 0; i < chatData.chats.Count; i++)
		{
			ChatData.Chat chat = chatData.chats[i];
			chat.chatWindow.SetReplyButton(false);
			chat.chatButton.SetNotification(false);
			for (int j = 0; j < chatData.chats[i].conversations.Count; j++)
			{
				ConversationData conversation = chatData.chats[i].conversations[j];
				//INSTANTIATE CONVERSATIONS OF NONE
				if (conversation.trigger == ConversationData.Trigger.History)
				{
					DateTime date;
					if (SaveLoadManager.Instance.SaveGameSelected)
						date = SaveLoadManager.Instance.selectedSaveGame.creationDateTime.AddDays(UnityEngine.Random.Range(-5, -1));
					else
						date = DateTime.Now.AddDays(UnityEngine.Random.Range(-5, -1));
					InstantiateDate(chat, conversation, date.ToShortDateString());
					DateTime time = DateTime.Today.AddHours(7).AddMinutes(new System.Random().Next(241));
					foreach (ConversationData.ResponseNode responseNode in conversation.responseNodes)
					{
						time = time.AddMinutes((int)UnityEngine.Random.Range(1, 10));
						//I mean look at how horrible this gets! but no time to focus on this :( it works
						string contactName = GetContactName(conversation, responseNode);
						InstantiateChatText(chat, responseNode.Texts, time.ToShortTimeString(), leftMessagePrefab, contactName, NameColors[(int)responseNode.contact]);
						//set the online status to DateTime of last received response					
						if (!string.IsNullOrEmpty(responseNode.GetText))
						{
							chat.chatWindow.SetOnlineStatus(date);
						}
						time = time.AddMinutes((int)UnityEngine.Random.Range(1, 10));
						if (responseNode.options.Length > 0)
							InstantiateChatText(chat, responseNode.options[0].Texts, time.ToShortTimeString(), rightMessagePrefab);
					}


					ShowWindow(chatData.chats[i]);
					chat.chatButton.gameObject.SetActive(true);
					chat.chatButton.transform.SetAsFirstSibling();
				}
				//INSTANTIATE CONVERSATIONS OF SAVED HISTORY, IF SAVEGAME EXISTS
				else if (chatHistories[i].histories != null) //Instantiate the conversation of the savegame
				{
					foreach (ConversationData.History history in chatHistories[i].histories)
					{
						if (history == null || history.branches == null)
							continue;

						for (int k = 0; k < history.branches.Count; k++)
						{
							ConversationData.Branch branch = chatHistories[i].histories[j].branches[k];
							//If it's the first message of a conversation, instantiate the date bubble
							if (k == 0)
								InstantiateDate(chat, chat.conversations[j], branch.responseDateTime.ToShortDateString());
							string contactName = GetContactName(conversation, branch.responseNode);
							//Instantiate the chat messages if their text is not null
							if (!string.IsNullOrEmpty(branch.responseNode.GetText))
								InstantiateChatText(chat, branch.responseNode.Texts, branch.responseDateTime.ToShortTimeString(), leftMessagePrefab, contactName, NameColors[(int)branch.responseNode.contact]);
							if (branch.userOption != null && !string.IsNullOrEmpty(branch.userOption.GetText))
								InstantiateChatText(chat, branch.userOption.Texts, branch.optionDateTime.ToShortTimeString(), rightMessagePrefab);
						}
						if (history.LastDestinationID != 0) //Continue with that conversation if it was not finished
							RunChat(chat, conversation, conversation.responseNodes[history.LastDestinationID]);
						else
						{
							chat.chatWindow.SetReplyButton(false);
							DateTime dateTime = history.branches[history.branches.Count - 1].responseDateTime;
							chat.chatWindow.SetOnlineStatus(dateTime);
						}

					}
					ShowWindow(chatData.chats[i]);
					chat.chatButton.gameObject.SetActive(true);
					chat.chatButton.transform.SetAsFirstSibling();
				}
				//INSTANTIATE CONVERSATIONS OF NEW SAVE GAME
				else if (conversation.trigger == ConversationData.Trigger.Phase_0 && conversation.triggerRequirementAmount == 0)
				{
					RunChat(chat, conversation, conversation.responseNodes[0]);

				}
				chat.chatWindow.UpdateScroll();
			}
		}
	}


	private void InstantiateDate(ChatData.Chat contact, ConversationData conversationData, string date)
	{
		string[] _date = new string[] { date };
		InstantiateChatText(contact, _date, null, dateTextPrefab);
	}


	/// <summary>
	/// Checks if the chatwindow of personId is currently active and visible
	/// </summary>
	/// <param name="personId"></param>
	/// <returns></returns>
	bool IsChatWindowActive(ChatData.Chat contact)
	{
		return contact.chatWindow.IsVisible && Visible;
	}

	async void RunChat(ChatData.Chat chat, ConversationData conversationData, ConversationData.ResponseNode responseNode)
	{
		//Hide the Options panel
		chat.IsChatting = true;
		chat.chatWindow.SetOptionButtonState(false);
		string contactname = GetContactName(conversationData, responseNode);

		//Instantiate the centred date bubble if it's the first message of the conversation
		if (responseNode == conversationData.responseNodes[0])
			InstantiateDate(chat, conversationData, DateTime.Now.ToShortDateString());

		if (!string.IsNullOrEmpty(responseNode.GetText))
		{
			await UniTask.Delay(TimeSpan.FromSeconds(0.5f));
			chat.chatWindow.SetTypingIndicator(true);
			chat.chatWindow.UpdateScroll();
			if (IsChatWindowActive(chat))
				PlaySound(chatSounds.typing); //if the current window of the "typer" is active, play the typing sound
			await UniTask.Delay(TimeSpan.FromSeconds(responseNode.typeTime)); //fake typing time
			InstantiateChatText(chat, responseNode.Texts, DateTime.Now.ToShortTimeString(), leftMessagePrefab, contactname, NameColors[(int)responseNode.contact]);
			chat.chatWindow.SetTypingIndicator(false); //disable the typing bubble again and show the text
		}
		chat.chatWindow.UpdateScroll();

		//If the current response node has options, enable the reply button
		chat.chatWindow.SetReplyButton(responseNode.options.Length > 0);

		if (responseNode.options.Length > 0)
		{
			chat.chatWindow.SetOnlineStatus();
			for (int i = 0; i < responseNode.options.Length && i < chat.chatWindow.optionButtons.Count; i++)
				SetOptionButton(chat, conversationData, responseNode, responseNode.options[i], chat.chatWindow.optionButtons[i]);
		}
		else if (!IsLastResponse(conversationData, responseNode))
		{
			RunChat(chat, conversationData, NextResponse(conversationData, responseNode));
			AddToSaveData(chat, conversationData, responseNode, DateTime.Now);
		}
		else
		{
			chat.IsChatting = false;
			SetOnlineStatusDelayed(chat);
			AddToSaveData(chat, conversationData, responseNode, DateTime.Now);
		}

		if (chat.chatWindow.content.transform.childCount > 2)
		{
			if (!chat.muted)
			{
				if (IsChatWindowActive(chat))
					PlaySound(chatSounds.received);
				else
					PlaySound(chatSounds.notification);
			}
			chat.chatButton.SetNotification(true);
			desktopButton.SetNotification(true);

			chat.chatButton.gameObject.SetActive(true);
			chat.chatButton.transform.SetAsFirstSibling();
		}
	}

	ConversationData.ResponseNode NextResponse(ConversationData conversation, ConversationData.ResponseNode responseNode)
	{
		int responseId = 0;
		for (int i = 0; i < conversation.responseNodes.Count; i++)
		{
			if (responseNode == conversation.responseNodes[i])
			{
				responseId = i;
				break;
			}
		}
		return conversation.responseNodes[responseId + 1];
	}

	bool IsLastResponse(ConversationData conversation, ConversationData.ResponseNode responseNode)
	{
		return responseNode == conversation.responseNodes[conversation.responseNodes.Count - 1] || responseNode.endsConversation;
	}

	public void PlaySound(AudioClip clip)
	{
		Audio.clip = clip;
		Audio.Play();
	}


	public void InstantiateChatText(ChatData.Chat contact, string[] texts, string time, GameObject prefab, string contactName = null, Color nameColor = default)
	{
		if (string.IsNullOrEmpty(texts[0]))
			return;
		//if (text.Contains(".wav"))
		//{
		//	AudioClip clip = (AudioClip)Resources.Load("Audio/" + text.Replace(".wav", null));
		//	Instantiate(audioMessagePrefab, contact.chatWindow.content.transform).GetComponent<ChatAudioFile>().SetAudioMessage(clip, time);
		//}
		Instantiate(prefab, contact.chatWindow.content.transform).GetComponent<MessageBoxScaler>().SetMessage(texts, time, contactName, nameColor, GetKeyword(texts[0]));
		if (IsChatWindowActive(contact))
			contact.chatWindow.UpdateScroll();
	}

	public void TriggerConversation(ConversationData conversationData)
	{
		Debug.Log(conversationData.name);
		ChatData.Chat contact = null;
		for (int i = 0; i < chatData.chats.Count; i++)
		{
			for (int j = 0; j < chatData.chats[i].conversations.Count; j++)
				if (conversationData.name == chatData.chats[i].conversations[j].name)
				{
					contact = chatData.chats[i];
					//if (chatHistories[i].queuedConversationNames == null)
					//	chatHistories[i].queuedConversationNames = new Queue<string>();
					//chatHistories[i].queuedConversationNames.Enqueue(conversationData.name);
					break;
				}
			if (contact != null)
			{
				//if (!contact.IsChatting)
				//{
				RunChat(contact, conversationData, conversationData.responseNodes[0]);
				//	chatHistories[i].queuedConversationNames.Dequeue();
				//}
				break;
			}
		}
	}



	//Enables only one chatwindow and disables all other chatwindows
	public void ShowWindow(ChatData.Chat contact)
	{
		foreach (ChatData.Chat c in chatData.chats)
		{
			c.chatWindow.SetVisibility(contact.chatWindow == c.chatWindow);
			c.chatButton.button.interactable = contact.chatWindow != c.chatWindow;
		}
	}


	string GetKeyword(string text)
	{
		if (text.Contains("[USERNAME]"))
			return DesktopScreen.Instance.userName;
		if (text.Contains("[IP]"))
			return DesktopScreen.Instance.IP;
		if (text.Contains("[PW]"))
			return DesktopScreen.Instance.password;
		return null;
	}

	void ValidateNotification(ChatData.Chat chat)
	{
		if (!chat.IsChatting)
			chat.chatButton.SetNotification(false);
	}

	private void SetOptionButton(ChatData.Chat chat, ConversationData conversationData, ConversationData.ResponseNode responseNode, ConversationData.UserOption option, Button button)
	{
		//First remove all listeners of the button if it has been previously used
		button.onClick.RemoveAllListeners();
		button.gameObject.SetActive(true);
		button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = option.GetText.ReplaceKeywords();
		DateTime responseTime = DateTime.Now;
		button.onClick.AddListener(delegate
		{
			PlaySound(chatSounds.sent);
			InstantiateChatText(chat, option.Texts, DateTime.Now.ToShortTimeString(), rightMessagePrefab);
			chat.chatButton.SetNotification(false);
			ChatData.ChatHistory chatHistory = AddToSaveData(chat, conversationData, responseNode, responseTime, option);

			if (option.destinationID != 0)
				RunChat(chat, conversationData, conversationData.responseNodes[option.destinationID]);
			else if (!IsLastResponse(conversationData, responseNode))
			{
				RunChat(chat, conversationData, NextResponse(conversationData, responseNode));
			}
			else if (chatHistory.queuedConversationNames != null && chatHistory.queuedConversationNames.Count > 0)
			{
				ConversationData _conversationData = null;
				string conversationName = chatHistory.queuedConversationNames.Dequeue();
				for (int i = 0; i < chat.conversations.Count; i++)
				{
					if (chat.conversations[i].name == conversationName)
					{
						_conversationData = chat.conversations[i];
						break;
					}
				}
				RunChat(chat, _conversationData, _conversationData.responseNodes[0]);
			}

			if (option.destinationID == 0 && IsLastResponse(conversationData, responseNode) ||
			(chatHistory.queuedConversationNames == null ||
			chatHistory.queuedConversationNames.Count == 0))
			{
				chat.IsChatting = false;
				chat.chatWindow.SetOptionButtonState(false);
				SetOnlineStatusDelayed(chat);
			}

			chat.chatWindow.SetAnswerWindow();
			chat.chatWindow.SetReplyButton(false);
		});
	}

	ChatData.ChatHistory AddToSaveData(ChatData.Chat chat, ConversationData conversationData, ConversationData.ResponseNode responseNode, DateTime responseTime, ConversationData.UserOption option = null)
	{
		ConversationData.Branch branch = new ConversationData.Branch(responseNode, responseTime, option, DateTime.Now);
		ChatData.ChatHistory chatHistory = null;
		for (int i = 0; i < chatHistories.Length; i++)
		{
			if (chat.chatName == chatHistories[i].Name)
			{
				chatHistory = chatHistories[i];
				if (chatHistory.histories == null)
				{
					chatHistory.histories = new ConversationData.History[chat.conversations.Count];
				}
				for (int j = 0; j < chat.conversations.Count; j++)
				{
					if (chat.conversations[j] == conversationData)
					{
						if (chatHistory.histories[j] == null)
							chatHistory.histories[j] = new ConversationData.History();
						if (chatHistory.histories[j].branches == null)
							chatHistory.histories[j].branches = new List<ConversationData.Branch>();
						chatHistory.histories[j].branches.Add(branch);
						break;
					}
				}
				break;
			}
		}
		return chatHistory;
	}

	async void SetOnlineStatusDelayed(ChatData.Chat chat)
	{
		DateTime now = DateTime.Now;
		await UniTask.Delay(TimeSpan.FromSeconds(10));
		chat.chatWindow.SetOnlineStatus(now);
	}
}
