﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
[CustomEditor(typeof(ChatData))]
public class ChatDataEditor : Editor
{
    public SerializedProperty chats;
    void OnEnable()
    {
        chats = serializedObject.FindProperty("chats");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        ArrayGUI();
        serializedObject.ApplyModifiedProperties();
    }

    private void ArrayGUI()
    {
        for (int i = 0; i < chats.arraySize; i++)
        {
            SerializedProperty prop = chats.GetArrayElementAtIndex(i);
            EditorGUILayout.PropertyField(prop,  true);
            var style = new GUIStyle(GUI.skin.button);
            style.normal.textColor = Color.red;
            if (prop.isExpanded)
                if (GUILayout.Button("Remove Chat", style))
                {
                    chats.DeleteArrayElementAtIndex(i);
                }
            EditorGUILayout.Space();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Chat"))
        {
            chats.arraySize++;
        }
        GUILayout.EndHorizontal();
    }
}
#endif