﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UniRx.Async;
public class ChatWindow : MonoBehaviour
{
	public GameObject content;
	public GameObject typingIndicator;
	public List<Button> optionButtons;
	public ScrollRect scrollrect;
	public RectTransform containerRect;
	public RectTransform optionsRect;
	public Image profilePicture;
	public TextMeshProUGUI nameText;
	public TextMeshProUGUI onlineStatusText;

	public Button replyButton;
	public Image replyIcon;

	private Color replyIconColor;
	private ChatData.Chat chat;


	CanvasGroup _canvasGroup;
	CanvasGroup CanvasGroup
	{
		get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); }
	}


	public bool IsVisible
	{
		get { return CanvasGroup.alpha == 1; }
	}

	private void Start()
	{
		replyIconColor = replyIcon.color;
		replyButton.onClick.AddListener(SetAnswerWindow);
	}

	public void SetWindow(ChatData.Chat chat)
	{
		this.chat = chat;
		profilePicture.sprite = chat.profilePicture;
		nameText.text = chat.chatName;
	}

	public void SetOptionButtonState(bool val)
	{
		foreach (Button button in optionButtons)
			button.gameObject.SetActive(val);
	}

	public void SetReplyButton(bool val)
	{
		replyButton.interactable = val;
		if (val)
		{
			replyIcon.color = Color.white;
			replyIcon.DOColor(replyIconColor, 0.5f).SetLoops(-1, LoopType.Yoyo);
		}
		else
		{
			replyIcon.DOKill();
			replyIcon.DOColor(Color.white, 0.5f);
		}
	}

	public void SetOnlineStatus(System.DateTime dateTime = default)
	{
		onlineStatusText.text = null;
		if (chat.contacts.Length > 1)
		{
			for (int i = 0; i < chat.contacts.Length; i++)
				onlineStatusText.text += chat.contacts[i].ToString() + (i != chat.contacts.Length - 1 ? ", " : null);
		}
		else if (dateTime == default)
			onlineStatusText.text = "Online";
		else
			onlineStatusText.text = "Last seen: " + dateTime.ToShortDateString() + ", " + dateTime.ToShortTimeString();
	}

	/// <summary>
	/// Shows the typing indicator of the speaker
	/// </summary>
	/// <param name="val"></param>
	public void SetTypingIndicator(bool val)
	{
		if (val)
			typingIndicator.transform.SetAsLastSibling();
		typingIndicator.SetActive(val);
	}

	public bool OptionsVisible
	{
		get { return containerRect.offsetMin.y > 0; }
	}

	bool optionsVisible;
	public void SetAnswerWindow()
	{
		optionsVisible = !optionsVisible;
		if (optionsVisible)
			LeanTween.value(gameObject, containerRect.offsetMin.y, optionsRect.rect.height, 0.3f).setOnUpdate((float val) =>
			{
				containerRect.offsetMin = new Vector2(containerRect.offsetMin.x, val);
			});
		else
			LeanTween.value(gameObject, containerRect.offsetMin.y, 0, 0.3f).setOnUpdate((float val) =>
			{
				containerRect.offsetMin = new Vector2(containerRect.offsetMin.x, val);
			});
		ChatManager.Instance.desktopButton.SetNotification(false);
	}

	public void SetVisibility(bool val)
	{
		CanvasGroup.alpha = val ? 1 : 0;
		CanvasGroup.blocksRaycasts = val;
	}


	public void UpdateScroll()
	{
		LeanTween.cancel(gameObject);
		Canvas.ForceUpdateCanvases();
		LeanTween.value(gameObject, scrollrect.verticalNormalizedPosition, 0, 0.3f).setOnUpdate((float val) =>
		{
			scrollrect.verticalNormalizedPosition = val;
		});
	}

}
