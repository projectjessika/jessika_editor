﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
[CreateAssetMenu(fileName = "Conversation", menuName = "New Conversation")]
public class ConversationData : ScriptableObject
{
	public enum Trigger { Phase_0, Phase_1, Phase_2, Phase_3, Phase_4, History, None }
	public Trigger trigger;

	public int triggerRequirementAmount;

	public List<ResponseNode> responseNodes;

	public bool isGroupChat;

	[Serializable]
	public class ResponseNode
	{
		public ChatData.ContactNames contact;
		public string en;
		public string de;
		[Range(0.0f, 5.0f)]
		public float typeTime;
		public UserOption[] options;
		public bool canAnswer;
		public bool endsConversation;

		public string[] Texts { get { return new string[] { de, en }; } }
		public string GetText { get { return LocalizationManager.CurrentLanguageCode == "de" ? de : en; } }
	}

	[Serializable]
	public class UserOption
	{
		public string en;
		public string de;
		public int destinationID;
		public string[] Texts { get { return new string[] { de, en }; } }
		public string GetText { get { return LocalizationManager.CurrentLanguageCode == "de" ? de : en; } }
	}

	[Serializable]
	public class History
	{
		public List<Branch> branches = new List<Branch>();
		public int Length { get { return branches.Count; } }
		public int LastDestinationID { get { return (Length > 0 && branches[Length - 1].userOption != null) ? branches[branches.Count - 1].userOption.destinationID : 0; } }
	}

	[Serializable]
	public class Branch
	{
		public ResponseNode responseNode;
		public DateTime responseDateTime;
		public UserOption userOption;
		public DateTime optionDateTime;

		public Branch(ResponseNode responseNode, DateTime responseDateTime, UserOption chosenOption, DateTime optionDateTime)
		{
			this.responseNode = responseNode;
			this.responseDateTime = responseDateTime;
			this.userOption = chosenOption;
			this.optionDateTime = optionDateTime;
		}
	}

}
