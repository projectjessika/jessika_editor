﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RadialLoader : MonoBehaviour
{
    public Image circleImage;
    public CanvasGroup canvasGroup;

    private void OnEnable()
    {
        circleImage.fillAmount = 0;
        canvasGroup.DOFade(1, 0.5f);
        Rotate();
    }

    private void OnDisable()
    {
        circleImage.fillClockwise = false;
        circleImage.fillAmount = 0;
        canvasGroup.alpha = 0;
        circleImage.DOKill();
        transform.DOKill();
    }

    void Rotate()
    {
        circleImage.fillClockwise = !circleImage.fillClockwise;
        circleImage.DOFillAmount(circleImage.fillAmount == 1 ? 0 : 1, 1f).OnComplete(Rotate);
        transform.DOBlendableRotateBy(new Vector3(0, 0, -180), 1f);
    }
}
