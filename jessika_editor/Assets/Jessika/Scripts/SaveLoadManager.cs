﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

public class SaveLoadManager : MonoBehaviour
{
	public static SaveLoadManager Instance;
	string saveGamePath;
	BinaryFormatter bf;
	FileStream stream;
	DirectoryInfo dInfo;

	ChatManager _chatManager;
	ChatManager ChatManager
	{
		get
		{
			if (_chatManager == null)
			{
				_chatManager = FindObjectOfType<ChatManager>();
				return _chatManager;
			}
			else
				return _chatManager;
		}
	}

	DesktopScreen _desktopScreen;
	DesktopScreen DesktopScreen
	{
		get
		{
			if (_desktopScreen == null)
			{
				_desktopScreen = FindObjectOfType<DesktopScreen>();
				return _desktopScreen;
			}
			else
				return _desktopScreen;
		}
	}

	DecrypterManager _decrypterManager;
	DecrypterManager DecrypterManager
	{
		get
		{
			if (_decrypterManager == null)
			{
				_decrypterManager = FindObjectOfType<DecrypterManager>();
				return _decrypterManager;
			}
			else
				return _decrypterManager;
		}
	}

	MailManager _mailManager;
	MailManager MailManager
	{
		get
		{
			if (_mailManager == null)
			{
				_mailManager = FindObjectOfType<MailManager>();
				return _mailManager;
			}
			else
				return _mailManager;
		}
	}

	[Serializable]
	public class SaveGame
	{
		public string name;
		public DateTime creationDateTime;
		public DateTime lastAccessedDateTime;
		public PlayerData data;

		public SaveGame(string name, DateTime creationDateTime, DateTime lastAccessedDateTime, PlayerData data)
		{
			this.name = name;
			this.creationDateTime = creationDateTime;
			this.lastAccessedDateTime = lastAccessedDateTime;
			this.data = data;
		}

		public string GetPlayerName
		{
			get { return name.Replace(".sav", ""); }
		}
	}
	[SerializeField]
	public List<SaveGame> SaveGames;

	private void Awake()
	{
		Instance = this;
		saveGamePath = Application.persistentDataPath + "/Savegames/";
		if (!Directory.Exists(saveGamePath))
			Directory.CreateDirectory(saveGamePath);
		Debug.Log(saveGamePath);
		LoadSaveGames();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha0))
			Save();
	}

	private void LoadSaveGames()
	{
		//_Gameplay = null;
		_desktopScreen = null;
		_chatManager = null;
		_decrypterManager = null;
		_mailManager = null;

		Debug.Log("Updating save game list");
		dInfo = new DirectoryInfo(saveGamePath);
		SaveGames.Clear();
		// Add file sizes.
		FileInfo[] fis = dInfo.GetFiles();
		Array.Sort(fis, (x, y) => StringComparer.OrdinalIgnoreCase.Compare(y.CreationTime, x.CreationTime));

		foreach (FileInfo fi in fis)
			if (fi.Extension.Equals(".sav", StringComparison.OrdinalIgnoreCase))
			{
				SaveGame saveGame = new SaveGame(fi.Name, fi.CreationTime, fi.LastWriteTime, GetData(fi.Name));
				SaveGames.Add(saveGame);
			}
	}

	public void Delete(SaveGame saveGame)
	{
		if (File.Exists(saveGamePath + saveGame.name))
		{
			File.Delete(saveGamePath + saveGame.name);
			LoadSaveGames();
		}
		else
		{
			Debug.Log("Nothing to delete");
			return;
		}
	}

	string saveFileName;
	public void Save()
	{
		saveFileName = DesktopScreen.Instance.playerName + ".sav";
		SaveProgress();
	}


	private void SaveProgress()
	{
		string path = saveGamePath + saveFileName;
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(path, FileMode.OpenOrCreate);
		PlayerData data = new PlayerData(ChatManager, DesktopScreen, DecrypterManager, MailManager, Settings.Instance);
		bf.Serialize(file, data);
		file.Dispose();
		Debug.Log("Saved in " + path);
		LoadSaveGames();
	}

	public bool SaveGameSelected
	{
		get { return selectedSaveGame != null; }
	}

	public SaveGame selectedSaveGame;
	public void SetSelectedSaveGame(SaveGame saveGame)
	{
		selectedSaveGame = saveGame;
	}
	PlayerData GetData(string saveGameName)
	{
		bf = new BinaryFormatter();
		stream = new FileStream(saveGamePath + saveGameName, FileMode.Open);
		PlayerData data = bf.Deserialize(stream) as PlayerData;
		stream.Close();
		return data;
	}

	[System.Serializable]
	public class PlayerData
	{
		//General Player Parameters
		public string playerName;
		public ChatData.ChatHistory[] chatHistories;
		public List<MailManager.MailSaveData> mailSaveData;
		public DecrypterManager.SaveData decrypterSaveData;

		public string systemLanguage;
		public string subtitleLanguage;
		public string videoLanguage;

		public string IP;
		public string username;
		public string password;

		public PlayerData(ChatManager chatManager, DesktopScreen desktopScreen, DecrypterManager decrypterManager, MailManager mailManager, Settings settings)
		{
			playerName = desktopScreen.playerName;
			chatHistories = chatManager.chatHistories;
			decrypterSaveData = decrypterManager.saveData;
			mailSaveData = mailManager.mailSaveDatas;

			systemLanguage = settings.SystemLanguage;
			subtitleLanguage = settings.SubtitleLanguage;
			videoLanguage = settings.AudioLanguage;

			IP = desktopScreen.IP;
			username = desktopScreen.userName;
			password = desktopScreen.password;
		}
	}
}
