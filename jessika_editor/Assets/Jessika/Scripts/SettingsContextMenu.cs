﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class SettingsContextMenu : ContextWindow
{
    public static SettingsContextMenu Instance;

    public TextMeshProUGUI playerNameText;
    public Button saveButton;
    public Button logoutButton;
    public Button exitButton;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    { 
        saveButton.onClick.AddListener(Save);
        logoutButton.onClick.AddListener(Logout);
        exitButton.onClick.AddListener(Exit);
    }

    private void Save()
    {
        OverlayPanelBehaviour.Instance.ShowSavePanel();
    }

    private void Logout()
    {
        OverlayPanelBehaviour.Instance.ShowLogoutPanel();
    }

    private void Exit()
    {
        OverlayPanelBehaviour.Instance.ShowExitPanel();
    }

    public override void SetWindow()
    {
        base.SetWindow();
        playerNameText.text = DesktopScreen.Instance.playerName;
    }
}
