﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LanguageContextMenu : ContextWindow
{

	public static LanguageContextMenu Instance;

	public Button systemButton;
	public Button subtitleButton;
	public Button audioButton;

	public TextMeshProUGUI subtitleText;
	public TextMeshProUGUI audioText;


	private void Awake()
	{
		Instance = this;
	}

	public override void SetWindow()
	{
		base.SetWindow();
		SetSubtitleText();
		SetAudioText();
	}

	private void Start()
	{
		systemButton.onClick.AddListener(Settings.Instance.SetSystemLanguage);
		audioButton.onClick.AddListener(SetAudioLanguage);
		subtitleButton.onClick.AddListener(SetSubtitleLanguage);
	}


	void SetAudioLanguage()
	{
		Settings.Instance.AudioLanguage = Settings.Instance.AudioLanguage == "en" ? "de" : "en";
		SetAudioText();
	}

	void SetSubtitleLanguage()
	{
		Settings.Instance.SubtitleLanguage = Settings.Instance.SubtitleLanguage == "en" ? "de" : "en";
		SetSubtitleText();
	}

	void SetSubtitleText()
	{
		if (Settings.Instance.SubtitleLanguage == "en")
			subtitleText.text = "Subtitles: <b><u>EN</u></b>/DE";
		else
			subtitleText.text = "Untertitel: EN/<b><u>DE</u></b>";
	}

	void SetAudioText()
	{
		if (Settings.Instance.AudioLanguage == "en")
			audioText.text = "Audio: <b><u>EN</u></b>/DE";
		else
			audioText.text = "Audio: EN/<b><u>DE</u></b>";
	}
}
