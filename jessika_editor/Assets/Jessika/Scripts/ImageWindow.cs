﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ImageWindow : WindowBehaviour
{
    FileButton fileButton;
    public Image image;

    public override void SetMediaFile(FileButton fileButton)
    {
        image.sprite = fileButton.fileData.picture;
    }
}
