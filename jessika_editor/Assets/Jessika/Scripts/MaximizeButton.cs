﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MaximizeButton : Button
{
	WindowBehaviour _windowBehaviour;
	WindowBehaviour windowBehaviour
	{
		get { return _windowBehaviour = _windowBehaviour ?? GetComponentInParent<WindowBehaviour>(); }
	}

	protected override void Start()
	{
		onClick.AddListener(Maximize);

	}
	void Maximize()
	{
		windowBehaviour.SetWindowSize();
	}

	void SetInteraction()
	{
		interactable = !windowBehaviour.IsMaximized;
	}
}
