﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using UniRx.Async;
using System;

public class OverlayPanelBehaviour : MonoBehaviour
{
	public static OverlayPanelBehaviour Instance;

	public CanvasGroup savePanelCG;
	public CanvasGroup logoutPanelCG;
	public CanvasGroup exitPanelCG;

	public GameObject saveRadialLoader;
	public TextMeshProUGUI saveText;

	public Button logoutCancelButton;
	public Button logoutYesButton;
	public Button logoutNoButton;

	public Button exitCancelButton;
	public Button exitYesButton;
	public Button exitNoButton;

	private CanvasGroup _canvasGroup;
	private CanvasGroup CanvasGroup
	{
		get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); }
	}

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		SetButtons();
	}

	public async void ShowSavePanel()
	{
		Show();
		await IndicateSaveProgress();
		Hide();
	}

	public void ShowLogoutPanel()
	{
		Show();
		SetPanels(logoutPanelCG);
	}

	public void ShowExitPanel()
	{
		Show();
		SetPanels(exitPanelCG);
	}

	private void SetPanels(CanvasGroup canvasGroup)
	{
		if (canvasGroup)
		{
			canvasGroup.interactable = true;
			canvasGroup.blocksRaycasts = true;
		}
		savePanelCG.DOFade(canvasGroup == savePanelCG ? 1 : 0, 0.3f);
		logoutPanelCG.DOFade(canvasGroup == logoutPanelCG ? 1 : 0, 0.3f);
		exitPanelCG.DOFade(canvasGroup == exitPanelCG ? 1 : 0, 0.3f);

	}

	private async UniTask IndicateSaveProgress()
	{
		SaveLoadManager.Instance.Save();
		saveRadialLoader.SetActive(true);
		saveText.gameObject.SetActive(false);
		SetPanels(savePanelCG);
		await UniTask.Delay(TimeSpan.FromSeconds(1f));
		saveRadialLoader.SetActive(false);
		saveText.gameObject.SetActive(true);
		await UniTask.Delay(TimeSpan.FromSeconds(1f));
	}

	private async void SaveAndLogout()
	{
		await IndicateSaveProgress();
		UICanvasController.Instance.ShowMenuScreen();
	}

	private async void SaveAndExit()
	{
		await IndicateSaveProgress();
		Debug.Log("Quit");
		Application.Quit();
	}

	private void Show()
	{
		SetOverlayPanel(true);
	}

	private void Hide()
	{
		SetOverlayPanel(false);
		SetPanels(null);
	}

	private void SetOverlayPanel(bool visible)
	{
		CanvasGroup.DOFade(visible ? 1 : 0, 0.3f);
		CanvasGroup.interactable = visible;
		CanvasGroup.blocksRaycasts = visible;
	}

	private void SetButtons()
	{
		logoutCancelButton.onClick.AddListener(Hide);
		logoutYesButton.onClick.AddListener(SaveAndLogout);
		logoutNoButton.onClick.AddListener(UICanvasController.Instance.ShowMenuScreen);

		exitCancelButton.onClick.AddListener(Hide);
		exitYesButton.onClick.AddListener(SaveAndExit);
		exitNoButton.onClick.AddListener(Application.Quit);
	}

}
