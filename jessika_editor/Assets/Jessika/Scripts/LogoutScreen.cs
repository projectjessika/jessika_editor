﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoutScreen : MonoBehaviour
{
	SaveLoadManager _SaveLoad;
	SaveLoadManager SaveLoad
	{
		get { return _SaveLoad = _SaveLoad ?? FindObjectOfType<SaveLoadManager>(); }
	}
	public Button YesButton;
	public Button NoButton;
	public Button CancelButton;

	private void Start()
	{
		YesButton.onClick.AddListener(delegate
		{
			SaveLoad.Save();
			Debug.Log("Save and Logout");
		});
		NoButton.onClick.AddListener(delegate
		{
			Debug.Log("Continue withhout saving");
		});
		CancelButton.onClick.AddListener(delegate
		{
			Debug.Log("Cancel Logout");
			gameObject.SetActive(false);
		});
	}
}
