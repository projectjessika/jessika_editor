﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDOL : MonoBehaviour {

	public void Awake()
	{
		DontDestroyOnLoad(gameObject);
		Debug.Log("DontDestroyOnLoad: " + gameObject.name);
		if (FindObjectsOfType(GetType()).Length > 1)
		{
			Destroy(gameObject);
		}
	}
}
