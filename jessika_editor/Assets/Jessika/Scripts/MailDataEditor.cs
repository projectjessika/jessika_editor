﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
[CustomEditor(typeof(MailData))]
public class MailDataEditor : Editor
{
	public MailData mailData;
	public SerializedProperty mails;
	GUIStyle buttonStyle;
	void OnEnable()
	{
		mails = serializedObject.FindProperty("mails");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		ArrayGUI();
		serializedObject.ApplyModifiedProperties();
	}


	private void ArrayGUI()
	{
		mailData = target as MailData;

		buttonStyle = new GUIStyle(GUI.skin.button);
		buttonStyle.normal.textColor = Color.red;

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Mail"))
		{
			mails.arraySize++;
		}
		GUILayout.EndHorizontal();

		if (mailData.mails == null)
			return;
		for (int i = 0; i < mailData.mails.Count; i++)
		{
			GUILayout.BeginVertical("GroupBox");
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(mails.GetArrayElementAtIndex(i), new GUIContent("Mail " + (i).ToString() + ": "), false);
			SerializedProperty mail = mails.GetArrayElementAtIndex(i);
			if (mail.isExpanded)
				if (GUILayout.Button("Remove", buttonStyle))
				{
					mails.DeleteArrayElementAtIndex(i);
					break;
				}
			GUILayout.EndHorizontal();
			if (mail.isExpanded)
			{
				EditorGUILayout.Space();
				DisplayMail(mailData.mails[i], mail);
			}
			GUILayout.EndHorizontal();
		}



	}

	void DisplayMail(MailData.Mail mail, SerializedProperty prop)
	{
		mail.correspondent = EditorGUILayout.TextField("Correspondent", mail.correspondent);
		mail.subjectEN = EditorGUILayout.TextField("Subject EN", mail.subjectEN);
		mail.subjectDE = EditorGUILayout.TextField("Subject DE", mail.subjectDE);
		mail.isSpam = EditorGUILayout.Toggle("Spam", mail.isSpam);
		EditorGUILayout.PropertyField(prop.FindPropertyRelative("trigger"), new GUIContent("Trigger"));

		if (mail.trigger != MailData.Trigger.None)
			mail.triggerRequirementAmount = EditorGUILayout.IntSlider("Trigger file amount", mail.triggerRequirementAmount, 0, 20);
		else
			mail.triggerRequirementAmount = 0;

		SerializedProperty contents = prop.FindPropertyRelative("contents");
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Content"))
		{
			contents.arraySize++;
		}
		GUILayout.EndHorizontal();

		DisplayContent(mail, contents);

	}

	void DisplayContent(MailData.Mail mail, SerializedProperty prop)
	{
		for (int i = 0; i < mail.contents.Count; i++)
		{
			MailData.Content content = mail.contents[i];
			SerializedProperty contentProp = prop.GetArrayElementAtIndex(i);

			GUILayout.BeginVertical("GroupBox");
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(prop.GetArrayElementAtIndex(i), new GUIContent("Content " + (i).ToString() + ": "), false);
			if (contentProp.isExpanded)
				if (GUILayout.Button("Remove", buttonStyle))
				{
					prop.DeleteArrayElementAtIndex(i);
					break;
				}
			GUILayout.EndHorizontal();
			if (contentProp.isExpanded)
			{
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(contentProp.FindPropertyRelative("contentType"), new GUIContent("Type"));
				if (content.contentType == MailData.ContentType.picture)
				{
					content.picture = (Sprite)EditorGUILayout.ObjectField("Picture", content.picture, typeof(Sprite));
					content.textEN = null;
					content.textDE = null;
				}
				else
				{
					content.textEN = EditorGUILayout.TextField("Text EN", content.textEN);
					content.textDE = EditorGUILayout.TextField("Text DE", content.textDE);
					content.picture = null;
				}
			}
			GUILayout.EndHorizontal();
		}

	}


	public void DrawUILine(Color color, int thickness = 2, int padding = 10)
	{
		Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
		r.height = thickness;
		r.y += padding / 2;
		r.x -= 2;
		r.width += 6;
		EditorGUI.DrawRect(r, color);
	}

}
#endif