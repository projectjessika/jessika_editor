﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
public class AudioVisualizer : MonoBehaviour
{
	//An AudioSource object so the music can be played  
	float minHeight = 15f;
	float maxHeight = 200f;
	float updateSensitivity = 0.25f;
	int visualizerSamples = 64;

	RectTransform[] _visualizerRT;
	RectTransform[] visualizersRT
	{
		get { return _visualizerRT = _visualizerRT ?? GetComponentsInChildren<RectTransform>(); }
	}

	AudioSource _audioSource;
	AudioSource AudioSource
	{ get { return _audioSource = _audioSource ?? GetComponentInParent<AudioSource>(); } }


	CanvasGroup _canvasGroup;
	CanvasGroup CanvasGroup
	{
		get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); }
	}

	private void OnEnable()
	{
		CanvasGroup.alpha = 0;
		CanvasGroup.DOFade(1f, 0.5f);
	}

	float[] SpectrumData
	{
		get { return AudioSource.GetSpectrumData(visualizerSamples, 0, FFTWindow.Rectangular); }
	}

	private void Update()
	{
		if (AudioSource.isPlaying)
		{
			for (int i = 0; i < visualizersRT.Length; i++)
			{
				Vector2 newSize = visualizersRT[i].rect.size;
				newSize.y = Mathf.Clamp(Mathf.Lerp(newSize.y, minHeight + (SpectrumData[i] * (maxHeight - minHeight) * 5), updateSensitivity), minHeight, maxHeight);
				visualizersRT[i].sizeDelta = newSize;
			}
		}
	}
}