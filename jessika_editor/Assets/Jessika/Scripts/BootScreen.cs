﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BootScreen : MonoBehaviour
{

	Text consoleText;
	// Use this for initialization
	void Start()
	{
		consoleText = GetComponentInChildren<Text>();
		StartCoroutine(StartBootText());
	}

	IEnumerator StartBootText()
	{
		consoleText.text += "Loading 'Jessika - Underneath the System' ...";
		yield return new WaitForSeconds(1f);
		consoleText.text += "\nA Game by TriTrie Games\n";
		yield return new WaitForSeconds(1f);
		consoleText.text += "\nLoading Team ...";
		yield return new WaitForSeconds(1f);
		consoleText.text += "\n[SEREN BESORAK] currently smoking.\n";
		yield return new WaitForSeconds(0.5f);
		consoleText.text += "[PIERRE SCHLÖMP] drinking fancy tea.\n";
		yield return new WaitForSeconds(0.5f);
		consoleText.text += "[SARAH ABOUZARI] in the matrix.\n";
		yield return new WaitForSeconds(1f);
		consoleText.text += "\nLoading Actress ...";
		yield return new WaitForSeconds(0.5f);
		consoleText.text += "\n[LISA SOPHIE KUSZ]\n\n";

		//Boot text
		yield return new WaitForSeconds(1f);
		consoleText.text += "YOS Version 2.4.18-15-3.y Uncompressing YOS... done, booting the kernel.\n";
		yield return new WaitForSeconds(0.1f);
		consoleText.text += "[0.000000] Booting Linux on physical CPU 0x0\n[0.000000] Initializing cgroup subsys cpu\n[0.000000] Initializing cgroup subsys cpuacct\n[0.000000] Linux version 3.18.10 + (dc4@dc4 - XPS13 - 9333) \n(gcc version 4.8.3 20140303(prerelease)(crosstool - NG linaro - 1.13.1 + bzr2650 - Linaro GCC 2014.03)) \n#775 PREEMPT ";
		yield return new WaitForSeconds(0.2f);
		consoleText.text += "1868K rodata, 340K init, 733K bss, 19236K reserved)\n[0.000000] Virtual kernel memory layout:\n[    0.000000] vector  : 0xffff0000 - 0xffff1000   (   4 kB)\n[    0.000000] fixmap  : 0xffc00000 - 0xffe00000   (2048 kB)\n[    0.000000] vmalloc : 0xcc800000 - 0xff000000   ( 808 MB)\n[    0.000000] lowmem  : 0xc0000000 - 0xcc000000   ( 192 MB)\n[    0.000000] modules : 0xbf000000 - 0xc0000000   (  16 MB)\n[    0.000000]       .text : 0xc0008000 - 0xc079a78c   (7754 kB)\n[    0.000000]       .init : 0xc079b000 - 0xc07f0000   ( 340 kB)\n[    0.000000]       .data : 0xc07f0000 - 0xc084711c   ( 349 kB)\n[    0.000000].bss : 0xc084711c - 0xc08fe848   ( 734 kB)\n";
		yield return new WaitForSeconds(0.2f);
		consoleText.text += "1868K rodata, 340K init, 733K bss, 19236K reserved)\n[0.000000] Virtual kernel memory layout:\n[    0.000000] vector  : 0xffff0000 - 0xffff1000   (   4 kB)\n[    0.000000] fixmap  : 0xffc00000 - 0xffe00000   (2048 kB)\n[    0.000000] vmalloc : 0xcc800000 - 0xff000000   ( 808 MB)\n[    0.000000] lowmem  : 0xc0000000 - 0xcc000000   ( 192 MB)\n[    0.000000] modules : 0xbf000000 - 0xc0000000   (  16 MB)\n[    0.000000]       .text : 0xc0008000 - 0xc079a78c   (7754 kB)\n[    0.000000]       .init : 0xc079b000 - 0xc07f0000   ( 340 kB)\n[    0.000000]       .data : 0xc07f0000 - 0xc084711c   ( 349 kB)\n[    0.000000].bss : 0xc084711c - 0xc08fe848   ( 734 kB)\n";
		yield return new WaitForSeconds(0.2f);

		consoleText.enabled = false;
		float time = 0;
		while (time < 1)
		{
			time += Time.deltaTime;
			yield return null;
		}
		gameObject.SetActive(false);
	}
}

