﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kino;
using TMPro;
using I2.Loc;
using DG.Tweening;
using UniRx.Async;

public class Terminal : MonoBehaviour
{
	public static Terminal Instance;

	public TextMeshProUGUI terminalText;
	public TerminalData terminalData;
	public string terminalTextSave;
	public bool terminalIsRunning;
	private int insertIndex;

	ScrollRect _scrollRect;
	ScrollRect scrollRect
	{
		get { return _scrollRect = _scrollRect ?? GetComponentInChildren<ScrollRect>(); }
	}

	TerminalInputField _terminalInputField;
	public TerminalInputField TerminalInputField
	{
		get { return _terminalInputField = _terminalInputField ?? GetComponentInChildren<TerminalInputField>(); }
	}

	private void Awake()
	{
		Instance = this;
	}
	private void Start()
	{
		ResetInput(false);
		if (!DecrypterManager.Instance.saveData.loggedIn)
			RequestIPAsync();
		else
		{
			terminalText.text = DecrypterManager.Instance.saveData.terminalTextSave;
			RequestTagAsync();
		}

		insertIndex = terminalText.text.Length;

		TerminalInputField.OnSubmitCommand += ValidateIP;
		TerminalInputField.OnSubmitCommand += ValidateTag;
		TerminalInputField.OnValueChanged += UpdateTerminalText;
		TerminalInputField.OnSelectTerminal += BlinkIndicator;
		TerminalInputField.OnDeselectTerminal += CancelIndicator;
	}

	public void Update()
	{
		if (DecrypterManager.Instance.IsTopWindow && !terminalIsRunning && (!TerminalInputField.isFocused || !TerminalInputField.isSelected || !TerminalInputField.IsActive()))
		{
			TerminalInputField.Select();
			TerminalInputField.ActivateInputField();
		}
	}


	async void RequestIPAsync()
	{
		await AddStrings(terminalData.loginIP.GetText);
		ResetInput(true);
	}

	async void ValidateIP(string command)
	{
		if (DecrypterManager.Instance.saveData.loggedIn)
			return;
		terminalIsRunning = true;
		if (string.IsNullOrEmpty(command) || command == " ")
			ResetInput(true);
		else
		{
			ResetInput(false);
			if (command == DesktopScreen.Instance.IP)
			{
				await AddStrings(terminalData.correctIP.GetText);
				DecrypterLogin.Instance.SetWindowState(true, false);
			}
			else
			{
				await AddStrings(terminalData.wrongIP.GetText);
				RequestIPAsync();
			}
		}
	}

	public async void CancelLogin()
	{
		await AddStrings(terminalData.cancelLogin.GetText);
		RequestIPAsync();
	}

	public async void CorrectLogin()
	{
		await AddStrings(terminalData.correctLogin.GetText);
		DecrypterManager.Instance.saveData.loggedIn = true;
		RequestTagAsync();
	}

	async void RequestTagAsync()
	{
		await AddStrings(terminalData.tagInput.GetText);
		ResetInput(true);
	}

	async void ValidateTag(string command)
	{
		if (!DecrypterManager.Instance.saveData.loggedIn)
			return;
		terminalIsRunning = true;
		ResetInput(false);
		await DecrypterManager.Instance.Search(command);
		if (DecrypterManager.Instance.matches.Count == 0)
			await AddStrings(terminalData.noResult.GetText);
		else
			await AddStrings(terminalData.hasResult.GetText);
		DecrypterManager.Instance.saveData.terminalTextSave = terminalText.text;
		RequestTagAsync();
	}


	void UpdateTerminalText(string command)
	{
		terminalText.text = terminalText.text.Replace("|", "");
		string replacedString = GetReplacedString(TerminalInputField.text);
		TerminalInputField.text = replacedString;
		terminalText.text = terminalText.text.Substring(0, terminalText.text.Length - (terminalText.text.Length - insertIndex));
		terminalText.text += replacedString;
	}

	void BlinkIndicator()
	{
		if (!TerminalInputField.isSelected || TerminalInputField.interactable == false)
			CancelIndicator();
		else
		{
			CancelIndicator();
			terminalText.text += "|";
			LeanTween.value(terminalText.gameObject, 0, 1f, 0.5f).setOnUpdate((float val) =>
			{
				if (val == 0 || val == 1)
				{
					if (terminalText.text.Contains("|"))
						terminalText.text = terminalText.text.Replace("|", "");
					else
						terminalText.text += "|";
				}
			}).setLoopPingPong();
		}
	}

	void CancelIndicator()
	{
		LeanTween.cancel(terminalText.gameObject);
		terminalText.text = terminalText.text.Replace("|", "");
	}

	public void ResetInput(bool interactable)
	{
		TerminalInputField.interactable = interactable;
		if (interactable)
		{
			TerminalInputField.text = null;
			terminalIsRunning = false;
			if (DecrypterManager.Instance.IsTopWindow)
			{
				TerminalInputField.Select();
				TerminalInputField.ActivateInputField();
			}
		}
	}


	async UniTask AddStrings(List<string> commandLines)
	{
		for (int i = 0; i < commandLines.Count; i++)
		{
			terminalText.text += "\n" + GetReplacedString(commandLines[i]);
			insertIndex = terminalText.text.Length;
			Canvas.ForceUpdateCanvases();
			scrollRect.verticalNormalizedPosition = 0;
			await UniTask.Delay(TimeSpan.FromSeconds(0.5f));
		}
	}

	string GetReplacedString(string line)
	{
		line = line.Replace("[DATE]", DateTime.Now.ToLongDateString());
		line = line.Replace("[TIME]", DateTime.Now.ToLongTimeString());
		line = line.Replace("[CMD]", TerminalInputField.text);
		line = line.Replace("[PLAYERNAME]", DesktopScreen.Instance.playerName.ToUpper());
		line = line.Replace("[IP]", DesktopScreen.Instance.IP);
		line = line.Replace("[USERNAME]", DesktopScreen.Instance.userName.ToUpper());
		line = line.Replace("[PW]", DesktopScreen.Instance.password);
		line = line.Replace("[SEEN]", DecrypterManager.Instance.saveData.openedFileNames.Count.ToString());
		line = line.Replace("[TOOL]", DecrypterManager.Instance.windowTitle.text);
		line = line.Replace("[FILESCOUNT]", DecrypterManager.Instance.saveData.discoveredFileNames.Count.ToString());
		line = line.Replace("[ENCRYPTEDCOUNT]", DecrypterManager.Instance.EncryptedFilesCount.ToString());
		line = line.Replace("[DECRYPTEDCOUNT]", DecrypterManager.Instance.DecryptedFilesCount.ToString());
		line = line.Replace("[MATCHES]", DecrypterManager.Instance.matches.Count.ToString());
		line = line.Replace("[...]", UnityEngine.Random.Range(10, 500).ToString());
		return line;
	}
}
