﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TagButton : MonoBehaviour
{
	public string tag;
	public TMPro.TextMeshProUGUI tagText;

	public void SetTagButton(string tag)
	{
		this.tag = tag;
		tagText.text = "#" + tag;
	}

	Button _button;
	Button button
	{
		get { return _button = _button ?? GetComponent<Button>(); }
	}

	// Use this for initialization
	void Start()
	{
		button.onClick.AddListener(delegate
		{
			DecrypterManager.Instance.Search(tag);
		});
	}
}
