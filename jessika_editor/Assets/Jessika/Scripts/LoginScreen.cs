﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class LoginScreen : ScreenCanvas
{
	public static LoginScreen Instance;

	public CanvasGroup screenCanvasGroup, loginCanvasGroup, agbCanvasGroup;

	public TMP_InputField userNameInput;
	public TMP_InputField passwordInput;

	public Button cancelAGBButton;
	public Button confirmButton;
	public Button loginButton;
	public Button cancelLoginButton;
	public ScrollRect scrollRect;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		cancelAGBButton.onClick.AddListener(delegate
		{
			UICanvasController.Instance.ShowMenuScreen();
		});
		confirmButton.onClick.AddListener(delegate
		{
			SetLoginPanel(true);
			SetAGBPanel(false);
		});
		loginButton.onClick.AddListener(ValidateInputs);
		cancelLoginButton.onClick.AddListener(delegate
		{
			SetLoginPanel(false);
			SetAGBPanel(true);
		});
	}

	private void Update()
	{

		if (Input.GetKeyDown(KeyCode.Tab) && userNameInput.isFocused)
			passwordInput.Select();
		if (Input.GetKeyDown(KeyCode.Return))
			ValidateInputs();
	}

	void ValidateInputs()
	{
		if (string.IsNullOrEmpty(userNameInput.text))
			ShakeInputField(userNameInput);
		else if (passwordInput.text != "password1234")
			ShakeInputField(passwordInput);
		else
			UICanvasController.Instance.ShowDesktopScreen(userNameInput.text);
	}

	void ShakeInputField(TMP_InputField inputField)
	{
		Color textColor = inputField.textComponent.color;
		Color placeholderColor = inputField.placeholder.color;
		inputField.transform.DOKill();
		inputField.transform.DOShakePosition(0.5f, 20, 10, 0);
		inputField.textComponent.DOColor(Color.red, 0.5f).OnComplete(() => inputField.textComponent.color = textColor);
		inputField.placeholder.DOColor(Color.red, 0.5f).OnComplete(() => inputField.placeholder.color = placeholderColor);
	}

	void SetLoginPanel(bool visible)
	{
		loginCanvasGroup.DOKill();
		loginCanvasGroup.blocksRaycasts = visible;
		loginCanvasGroup.DOFade(visible ? 1 : 0, 0.5f);
	}

	void SetAGBPanel(bool visible)
	{
		agbCanvasGroup.DOKill();
		agbCanvasGroup.blocksRaycasts = visible;
		agbCanvasGroup.DOFade(visible ? 1 : 0, 0.5f);
	}

}
