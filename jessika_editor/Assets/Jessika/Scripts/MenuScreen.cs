﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScreen : ScreenCanvas
{
    public static MenuScreen instance;
    public GameObject saveGameScrollRect;
    public GameObject saveGameButtonsContainer;
    public GameObject saveGameButtonPrefab;

    public Button addUserButton;
    public Button exitButton;
    public Button languageButton;

    private List<SaveGameButton> saveGameButtons = new List<SaveGameButton>();

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        SetButtons();
    }

    void SetButtons()
    {
        addUserButton.onClick.AddListener(delegate
        {
            UICanvasController.Instance.ShowLoginScreen();
        });

        exitButton.onClick.AddListener(delegate
        {
            Debug.Log("Quit");
            Application.Quit();
        });

        languageButton.onClick.AddListener(delegate
        {
            Settings.Instance.SetSystemLanguage();
            UpdateCanvas();
        });

        InstantiateSaveGameButtons();
    }

    public void InstantiateSaveGameButtons()
    {
        int saveGameCount = SaveLoadManager.Instance.SaveGames.Count;
        for (int i = 0; i < saveGameCount; i++)
        {
            if (saveGameButtons.Count <= i)
            {
                SaveGameButton saveGameButton = Instantiate(saveGameButtonPrefab, saveGameButtonsContainer.transform).GetComponent<SaveGameButton>();
                saveGameButtons.Add(saveGameButton);
            }
            saveGameButtons[i].gameObject.SetActive(true);
            saveGameButtons[i].SetSaveGameButton(SaveLoadManager.Instance.SaveGames[i]);
        }
        if (saveGameButtons.Count > saveGameCount)
        {
            for (int i = saveGameCount; i < saveGameButtons.Count; i++)
                saveGameButtons[i].gameObject.SetActive(false);
        }
        saveGameScrollRect.SetActive(saveGameCount > 0);
    }

    async void UpdateCanvas()
    {
        await UniTask.Delay(System.TimeSpan.FromSeconds(0.05f));
        ContentSizeFitter[] contentSizeFitters = FindObjectsOfType<ContentSizeFitter>();
        foreach (ContentSizeFitter contentSizeFitter in contentSizeFitters)
            contentSizeFitter.enabled = false;
        Canvas.ForceUpdateCanvases();
        foreach (ContentSizeFitter contentSizeFitter in contentSizeFitters)
            contentSizeFitter.enabled = true;
        Debug.Log("Canvas updated!");
    }
}
