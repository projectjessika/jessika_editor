﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSounds : MonoBehaviour
{
	public static AmbientSounds Instance;
	public GameObject bootScreen;
	public AudioSource keyboardAudioSource;
	public AudioSource loggingAudioSource;

	public List<AudioClip> keyboardSounds;
	public AudioClip loginSound;
	public AudioClip logoutSound;

	private void Awake()
	{
		Instance = this;
	}

	private void Update()
	{
		if (Input.anyKeyDown && !Input.GetMouseButton(0) && !Input.GetMouseButton(1))
			PlayKeySound(UnityEngine.Random.Range(0, keyboardSounds.Count));
	}

	void PlayKeySound(int key)
	{
		keyboardAudioSource.clip = keyboardSounds[key];
		keyboardAudioSource.Play();
	}

	public void Login()
	{
		loggingAudioSource.clip = loginSound;
		loggingAudioSource.Play();
	}

	public void Logout()
	{
		if (!loggingAudioSource)
			return;
		loggingAudioSource.clip = logoutSound;
		loggingAudioSource.Play();
	}
}
