﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UsernameInputField : TMP_InputField, IPointerClickHandler
{
	public override void OnPointerClick(PointerEventData eventData)
	{
		base.OnPointerClick(eventData);
		if (eventData.button == PointerEventData.InputButton.Right)
		{
			CopyPasteContextMenu.Instance.SetWindow(null, this);
		}
	}
}
