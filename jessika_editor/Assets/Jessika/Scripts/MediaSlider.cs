﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class MediaSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{

	bool interacting;
	public MediaPlayerWindow videoWindow;
	public ChatAudioFile audioControl;
	public Slider slider;
	float newSeconds;
	private void FixedUpdate()
	{
		if (!interacting)
		{
			if (videoWindow && (videoWindow.player.isPlaying || videoWindow.audioSrc.isPlaying))
				slider.value = 1.0f / videoWindow.ClipLength * videoWindow.CurrentTime;
			if (audioControl && audioControl.AudioSource.isPlaying)
				slider.value = 1.0f / (float)audioControl.audioClip.length * (float)audioControl.AudioSource.time;
		}
	}
	public void OnPointerDown(PointerEventData eventData)
	{
		interacting = true;
		if (videoWindow)
		{
			DesktopScreen.Instance.OnOtherMediaPlayed?.Invoke(videoWindow, true);
			newSeconds = slider.value * videoWindow.ClipLength;
			videoWindow.currentTime.text = System.String.Format("{0:00}:{1:00}", Mathf.Floor((float)newSeconds / 60), (float)newSeconds % 60);
		}
		if (audioControl)
			audioControl.AudioSource.Pause();
	}
	public void OnDrag(PointerEventData eventData)
	{
		interacting = true;
		if (videoWindow)
		{
			newSeconds = slider.value * videoWindow.ClipLength;
			videoWindow.currentTime.text = System.String.Format("{0:00}:{1:00}", Mathf.Floor((float)newSeconds / 60), (float)newSeconds % 60);
		}
		if (audioControl)
		{
			newSeconds = slider.value * (float)audioControl.audioClip.length;
			audioControl.audioTime.text = System.String.Format("{0:00}:{1:00}", Mathf.Floor((float)newSeconds / 60), (float)newSeconds % 60);
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (videoWindow)
			videoWindow.SetTime(newSeconds, true);
		if (audioControl)
			audioControl.SetTime(newSeconds);
		StartCoroutine(Wait());
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(0.2f);
		interacting = false;
	}
}
