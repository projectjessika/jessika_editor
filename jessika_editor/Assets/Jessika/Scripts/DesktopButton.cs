﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DesktopButton : MonoBehaviour
{
	public WindowBehaviour window;
	private Color origColor;

	Button _Button;
	Button Button
	{
		get { return _Button = _Button ?? GetComponent<Button>(); }
	}



	public void SetNotification(bool val)
	{
		if (!val)
		{
			Button.image.DOKill();
			Button.image.DOColor(origColor, 0.5f);
		}
		else
		{
			Button.image.DOColor(Color.green, 0.5f).SetLoops(-1, LoopType.Yoyo);
		}
	}


	// Use this for initialization
	void Start()
	{
		origColor = Button.image.color;
		Button.onClick.AddListener(delegate
		{
			if (!window.taskbarButton)
			{
				if (window.gameObject.activeSelf)
					SetWindow(false, false);
				else
					SetWindow(true, false);
			}
			else
				SetWindow(true, false);
			SetNotification(false);
		});

	}

	public void SetWindow(bool visible, bool minimized)
	{
		window.SetWindowState(visible, minimized);
	}

}