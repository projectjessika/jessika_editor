﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
public class DecrypterLogin : WindowBehaviour
{
	public static DecrypterLogin Instance;
	public TMP_InputField userNameInput;
	public TMP_InputField passwordInput;
	public Button confirmButton;
	public Button cancelButton;

	private void Awake()
	{
		Instance = this;
	}

	public override void Start()
	{
		base.Start();
		userNameInput.onEndEdit.AddListener(delegate
		{
			if (userNameInput.text == "[USERNAME]")
				userNameInput.text = DesktopScreen.Instance.userName;
		});
		passwordInput.onEndEdit.AddListener(delegate
		{
			if (passwordInput.text == "[PW]")
				passwordInput.text = DesktopScreen.Instance.password;
		});
		confirmButton.onClick.AddListener(delegate
		{
			ValidateInputs();
		});

		cancelButton.onClick.AddListener(Cancel);
		GetComponentInChildren<CloseButton>().onClick.AddListener(Cancel);
	}


	private void Update()
	{
		if (userNameInput.isFocused && Input.GetKeyDown(KeyCode.Tab))
			passwordInput.Select();
		if (Input.GetKeyDown(KeyCode.Return) && (userNameInput.isFocused || passwordInput.isFocused))
			ValidateInputs();
	}


	void ValidateInputs()
	{
		if (userNameInput.text != DesktopScreen.Instance.userName)
			ShakeInput(userNameInput);
		else if (passwordInput.text != DesktopScreen.Instance.password)
			ShakeInput(passwordInput);
		else
			Login();
	}

	void ShakeInput(TMP_InputField inputField)
	{
		Color textColor = inputField.textComponent.color;
		inputField.transform.DOKill();
		inputField.transform.DOShakePosition(0.5f, 20, 10, 0);
		inputField.textComponent.DOColor(Color.red, 0.5f).OnComplete(() => inputField.textComponent.color = textColor);
	}

	void Cancel()
	{
		SetWindowState(false, false);
		Terminal.Instance.CancelLogin();
	}

	void Login()
	{
		SetWindowState(false, false);
		Terminal.Instance.CorrectLogin();
	}

	public override void SetWindowState(bool visible, bool minimized)
	{
		base.SetWindowState(visible, minimized);
		if (!visible && !minimized)
		{
			userNameInput.text = null;
			passwordInput.text = null;
		}
	}

}
