﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx.Async;
using System;

public class TerminalInputField : InputField, IPointerClickHandler
{
	public Action<string> OnSubmitCommand;
	public Action<string> OnValueChanged;
	public Action OnSelectTerminal;
	public Action OnDeselectTerminal;
	public bool isSelected { get; private set; }
	private bool focused;

	// Start is called before the first frame update
	protected override void Start()
	{
		base.Start();
		onValueChanged.AddListener(delegate
		{
			OnValueChanged?.Invoke(text);
		});
		onValidateInput += CheckForEnter;
	}

	public override void OnPointerClick(PointerEventData eventData)
	{
		base.OnPointerClick(eventData);
		SelectTerminal();
		if (eventData.button == PointerEventData.InputButton.Right)
		{
			CopyPasteContextMenu.Instance.SetWindow(null, null, this);
		}
	}

	public override void OnSelect(BaseEventData eventData)
	{
		base.OnSelect(eventData);
		SelectTerminal();
	}
	public override void OnDeselect(BaseEventData eventData)
	{
		base.OnDeselect(eventData);
		DeselectTerminal();
	}

	public void SelectTerminal()
	{
		if (isSelected)
			return;
		isSelected = true;
		focused = true;
		OnSelectTerminal?.Invoke();
		if (!DecrypterManager.Instance.IsTopWindow)
			DecrypterManager.Instance.SetWindowState(true, false);
	}

	public void DeselectTerminal()
	{
		if (!isSelected)
			return;
		isSelected = false;
		DeactivateInputField();
		OnDeselectTerminal?.Invoke();
	}

	public override void Select()
	{
		base.Select();
		SelectTerminal();
	}


	private char CheckForEnter(string text, int charIndex, char addedChar)
	{
		if (addedChar == '\n')
		{
			OnSubmitCommand.Invoke(text);
			return '\0';
		}
		else
			return addedChar;
	}

	protected override void LateUpdate()
	{
		base.LateUpdate();
		if (focused)
		{
			MoveTextEnd(true);
			focused = false;
			if (!DecrypterManager.Instance.IsTopWindow)
				DecrypterManager.Instance.SetWindowState(true, false);
		}
	}

}
