﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Video;

[Serializable]
public class FileData 
{
	public enum FileType { Video, Picture, Text, Audio }

	//General data
	public string name;
	public Sprite thumbnail;
	public string tags;
	public bool isKeyFile;

	//Only one needed depending on filetype;
	public FileType fileType;
	public VideoClip video;
	public string videoTranslationEn;
	public string videoTranslationDe;
	public AudioClip dubEn;
	public Sprite picture;
	public string textEn;
	public string textDe;
	public AudioClip audio;

	//Some files might trigger a chat conversation
	public bool triggersChat;
	public ConversationData conversation;

	public bool IsVideo
	{
		get { return fileType == FileType.Video; }
	}

	public bool IsPicture
	{
		get { return fileType == FileType.Picture; }
	}

	public bool IsText
	{
		get { return fileType == FileType.Text; }
	}

	public bool IsAudio
	{
		get { return fileType == FileType.Audio; }
	}

	public bool TriggersConversation
	{
		get { return conversation != null; }
	}
}
