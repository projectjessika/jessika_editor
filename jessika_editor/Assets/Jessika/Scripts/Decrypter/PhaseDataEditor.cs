﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
[CustomEditor(typeof(PhaseData))]
public class PhaseDataEditor : Editor
{
	public SerializedProperty phases;
	GUIStyle labelStyle;
	PhaseData phaseData;

	void OnEnable()
	{
		phases = serializedObject.FindProperty("phases");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		ArrayGUI();
		serializedObject.ApplyModifiedProperties();

	}

	private void ArrayGUI()
	{
		phaseData = target as PhaseData;

		if (phaseData == null)
			return;

		var buttonStyle = new GUIStyle(GUI.skin.button);
		buttonStyle.normal.textColor = Color.red;

		labelStyle = new GUIStyle();
		labelStyle.fontStyle = FontStyle.Bold;

		for (int i = 0; i < phaseData.phases.Count; i++)
		{
			SerializedProperty phase = phases.GetArrayElementAtIndex(i);
			phaseData.phases[i].index = i;
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(phases.GetArrayElementAtIndex(i), new GUIContent("Phase " + (i).ToString() + ": "), false);
			if (phase.isExpanded)
				if (GUILayout.Button("Remove", buttonStyle))
				{
					phases.DeleteArrayElementAtIndex(i);
					break;
				}
			GUILayout.EndHorizontal();
			if (phase.isExpanded)
			{

				//----------------GENERAL THUMBNAILS------------------
				GUILayout.BeginVertical("GroupBox");
				phaseData.phases[i].thumbnailsExpanded = EditorGUILayout.Foldout(phaseData.phases[i].thumbnailsExpanded, "General Thumbnails", true);
				if (phaseData.phases[i].thumbnailsExpanded)
				{
					DrawUILine(new Color(0, 0, 0, 0.1f));
					phaseData.phases[i].encryptedThumbnail = (Sprite)EditorGUILayout.ObjectField("Encrypted", phaseData.phases[i].encryptedThumbnail, typeof(Sprite));
					phaseData.phases[i].decryptedThumbnail = (Sprite)EditorGUILayout.ObjectField("Decrypted", phaseData.phases[i].decryptedThumbnail, typeof(Sprite));
					phaseData.phases[i].corruptThumbnail = (Sprite)EditorGUILayout.ObjectField("Corrupt", phaseData.phases[i].corruptThumbnail, typeof(Sprite));
					phaseData.phases[i].mediaThumbnail = (Sprite)EditorGUILayout.ObjectField("Playable Media", phaseData.phases[i].mediaThumbnail, typeof(Sprite));
					phaseData.phases[i].imageThumbnail = (Sprite)EditorGUILayout.ObjectField("Image", phaseData.phases[i].imageThumbnail, typeof(Sprite));
					phaseData.phases[i].textThumbnail = (Sprite)EditorGUILayout.ObjectField("Text", phaseData.phases[i].textThumbnail, typeof(Sprite));
				}
				GUILayout.EndVertical();
				//--------------------------------------


				//----------------KEYFILE------------------
				GUILayout.BeginVertical("GroupBox");
				phaseData.phases[i].keyFileExpanded = EditorGUILayout.Foldout(phaseData.phases[i].keyFileExpanded, "Key File", true);
				if (phaseData.phases[i].keyFileExpanded)
				{
					DrawUILine(new Color(0, 0, 0, 0.1f));
					DisplayFileData(phaseData.phases[i].keyFile, true);
				}
				GUILayout.EndVertical();
				//--------------------------------------


				//----------------FILES------------------
				SerializedProperty files = phase.FindPropertyRelative("files");
				GUILayout.BeginVertical("GroupBox");
				GUILayout.BeginHorizontal();
				phaseData.phases[i].filesExpanded = EditorGUILayout.Foldout(phaseData.phases[i].filesExpanded, "Files", true);
				if (phaseData.phases[i].filesExpanded)
				{
					if (GUILayout.Button("Add File"))
						files.arraySize++;
				}
				GUILayout.EndHorizontal();
				if (phaseData.phases[i].filesExpanded)
				{
					DrawUILine(new Color(0, 0, 0, 0.1f));
					phaseData.phases[i].requirementCount = EditorGUILayout.IntSlider("Phase requirement count", phaseData.phases[i].requirementCount, 1, phaseData.phases[i].files.Count);
					for (int j = 0; j < phaseData.phases[i].files.Count; j++)
					{

						GUILayout.BeginVertical("GroupBox");
						SerializedProperty fileData = phase.FindPropertyRelative("files").GetArrayElementAtIndex(j);
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(fileData, new GUIContent("File " + (j).ToString() + ": "), false);
						if (fileData.isExpanded)
							if (GUILayout.Button("Remove", buttonStyle))
							{
								files.DeleteArrayElementAtIndex(j);
								break;
							}
						GUILayout.EndHorizontal();
						if (fileData.isExpanded)
						{
							EditorGUILayout.Space();
							DisplayFileData(phaseData.phases[i].files[j], false);
						}
						GUILayout.EndHorizontal();
					}
				}
				GUILayout.EndVertical();
				//--------------------------------------

			}
			EditorGUILayout.Space();
		}


		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Phase"))
		{
			phases.arraySize++;
		}
		GUILayout.EndHorizontal();
		if (GUI.changed)
			EditorUtility.SetDirty(phaseData);
	}

	public void DrawUILine(Color color, int thickness = 2, int padding = 10)
	{
		Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
		r.height = thickness;
		r.y += padding / 2;
		r.x -= 2;
		r.width += 6;
		EditorGUI.DrawRect(r, color);
	}

	void DisplayFileData(FileData fileData, bool isKeyFile)
	{
		FileData _fileData = fileData;
		_fileData.name = EditorGUILayout.TextField("File name", _fileData.name);
		_fileData.isKeyFile = isKeyFile;

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Type");
		_fileData.fileType = (FileData.FileType)EditorGUILayout.EnumPopup(_fileData.fileType); //EditorGUILayout.ObjectField("File type", keyFile.fileType) as enum;
		if (_fileData.IsVideo)
			_fileData.video = (UnityEngine.Video.VideoClip)EditorGUILayout.ObjectField(_fileData.video, typeof(UnityEngine.Video.VideoClip));
		else if (_fileData.IsPicture)
			_fileData.picture = (Sprite)EditorGUILayout.ObjectField(_fileData.picture, typeof(Sprite));
		else if (_fileData.IsAudio)
			_fileData.audio = (AudioClip)EditorGUILayout.ObjectField(_fileData.audio, typeof(AudioClip));
		EditorGUILayout.EndHorizontal();

		if (_fileData.IsVideo || _fileData.IsAudio)
		{
			_fileData.videoTranslationEn = EditorGUILayout.TextField("Translation En", _fileData.videoTranslationEn);
			_fileData.videoTranslationDe = EditorGUILayout.TextField("Translation De", _fileData.videoTranslationDe);
			_fileData.dubEn = (AudioClip)EditorGUILayout.ObjectField("English Dub", _fileData.dubEn, typeof(AudioClip));
		}

		if (_fileData.IsText)
		{
			_fileData.textDe = EditorGUILayout.TextField("Text De", _fileData.textDe);
			_fileData.textEn = EditorGUILayout.TextField("Text En", _fileData.textEn);
		}
		EditorGUILayout.Space();
		_fileData.tags = EditorGUILayout.TextField("Tags", _fileData.tags);
		_fileData.triggersChat = EditorGUILayout.Toggle("Triggers chat", _fileData.triggersChat);
		if (_fileData.triggersChat)
			_fileData.conversation = (ConversationData)EditorGUILayout.ObjectField("Conversation Data", _fileData.conversation, typeof(ConversationData));
	}

}
#endif
