﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;

[CreateAssetMenu(fileName = "TerminalData", menuName = "New Terminal Data")]
public class TerminalData : ScriptableObject
{
	[System.Serializable]
	public class TerminalText
	{
		public List<string> en;
		public List<string> de;

		public List<string> GetText
		{
			get { return LocalizationManager.CurrentLanguageCode == "de" ? de : en; }
		}
	}

	public TerminalText loginIP;
	public TerminalText wrongIP;
	public TerminalText correctIP;
	public TerminalText cancelLogin;
	public TerminalText correctLogin;

	public TerminalText tagInput;
	public TerminalText noResult;
	public TerminalText hasResult;
}
