﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Video;
[CreateAssetMenu(fileName = "Phases", menuName = "New Phases List")]
public class PhaseData : ScriptableObject
{

	[Serializable]
	public class Phase
	{
		public int index;
		[SerializeField]
		public Sprite encryptedThumbnail;
		[SerializeField]
		public Sprite decryptedThumbnail;
		[SerializeField]
		public Sprite corruptThumbnail;
		[SerializeField]
		public Sprite mediaThumbnail;
		[SerializeField]
		public Sprite imageThumbnail;
		[SerializeField]
		public Sprite textThumbnail;
		public FileData keyFile;
		public List<FileData> files;
		public int requirementCount;

		public bool thumbnailsExpanded;
		public bool keyFileExpanded;
		public bool filesExpanded;
	}

	public List<Phase> phases;

}
