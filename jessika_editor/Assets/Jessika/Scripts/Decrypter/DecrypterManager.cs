﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UniRx.Async;
using DG.Tweening;
using System;

public class DecrypterManager : WindowBehaviour
{
	public static DecrypterManager Instance;
	public PhaseData phaseData;

	//prefabs
	public MediaPlayerWindow videoWindowPrefab;
	public TextWindow textWindowPrefab;
	public ImageWindow imageWindowPrefab;

	public TaskbarButton taskbarButtonPrefab;
	public FileButton fileButtonPrefab;
	public TagButton tagButtonPrefab;

	//Containers
	public Transform tagButtonContainer;
	public Transform fileButtonContainer;

	//statistics texts
	public TextMeshProUGUI statisticsText;
	public TextMeshProUGUI loginText;

	public Image linearLoaderFill;

	public Button allButton;
	public Button repairedButton;
	public Button favsButton;


	public List<FileData> matches { get; private set; } = new List<FileData>();

	public bool FavWindowVisible
	{
		get { return !favsButton.interactable; }
	}

	private List<FileButton> fileButtons = new List<FileButton>();
	private List<FileButton> keyfileButtons = new List<FileButton>();
	private void Awake()
	{
		Instance = this;
		if (SaveLoadManager.Instance == null || !SaveLoadManager.Instance.SaveGameSelected)
			saveData = new SaveData();
		else
			saveData = SaveLoadManager.Instance.selectedSaveGame.data.decrypterSaveData;
	}

	public override void Start()
	{
		base.Start();
		CurrentPhase = GetCurrentPhase;
		allButton.onClick.AddListener(ShowResults);
		repairedButton.onClick.AddListener(ShowRestoredFiles);
		favsButton.onClick.AddListener(ShowFavFiles);
		InstantiateTagButtons();
		UpdateStatistics();
		InstantiateKeyfileButtons();
	}


	public int CurrentPhase { get; private set; }
	private int GetCurrentPhase
	{
		get
		{
			int currentPhase = 0;
			//reset the watch amount in each phase
			for (int i = 0; i < phaseData.phases.Count; i++)
			{
				int seenFiles = 0;
				for (int j = 0; j < phaseData.phases[i].files.Count; j++)
				{
					if (IsFileOpened(phaseData.phases[i].files[j].name))
						seenFiles += 1;
				}
				if (seenFiles >= phaseData.phases[i].requirementCount)
					currentPhase += 1;
			}
			return currentPhase;
		}
	}

	public bool IsFileOpened(string fileName)
	{
		return saveData != null && saveData.openedFileNames.Contains(fileName);
	}

	public Action<int, int> OnNewFileOpened;

	public void AddOpenedFile(string fileName)
	{
		if (IsFileOpened(fileName))
			return;
		saveData.openedFileNames.Add(fileName);
		if (GetCurrentPhase > CurrentPhase)
		{
			keyfileButtons[CurrentPhase].OpenFile();
			CurrentPhase = GetCurrentPhase;
			ShowRestoredFiles();
		}
		foreach (ChatData.Chat contact in ChatManager.Instance.chatData.chats)
			foreach (ConversationData conversation in contact.conversations)
				if ((int)conversation.trigger == CurrentPhase && conversation.triggerRequirementAmount == saveData.openedFileNames.Count)
					ChatManager.Instance.TriggerConversation(conversation);
		OnNewFileOpened?.Invoke(CurrentPhase, saveData.openedFileNames.Count);
		UpdateStatistics();
	}

	public void AddRemoveFavFile(FileData fileData)
	{
		if (IsFavFile(fileData.name))
			saveData.favsFileNames.Remove(fileData.name);
		else
			saveData.favsFileNames.Add(fileData.name);
		if (FavWindowVisible)
			ShowFavFiles();
		DesktopScreen.Instance.OnFavChanged?.Invoke(fileData);
	}

	public bool IsFavFile(string fileName)
	{
		return saveData.favsFileNames.Contains(fileName);
	}

	//Only applies to discovered files amount
	public int EncryptedFilesCount
	{
		get
		{
			int encrypted = 0;
			for (int i = CurrentPhase + 1; i < phaseData.phases.Count; i++)
			{
				for (int j = 0; j < phaseData.phases[i].files.Count; j++)
					if (saveData.discoveredFileNames.Contains(phaseData.phases[i].files[j].name))
						encrypted += 1;
			}
			return encrypted;
		}
	}

	//Only applies to discovered files amount
	public int DecryptedFilesCount
	{
		get
		{
			return saveData.discoveredFileNames.Count - EncryptedFilesCount;
		}
	}

	bool searching = false;

	public async UniTask Search(string searchInput)
	{
		if (searchInput == "," || searchInput == "." || string.IsNullOrEmpty(searchInput) || string.IsNullOrWhiteSpace(searchInput))
			return;
		searching = true;
		string search = searchInput.ToLower();

		SetFileTabButtons(allButton);
		matches = new List<FileData>();
		float searchTime = UnityEngine.Random.Range(0.5f, 3f);

		linearLoaderFill.DOFillAmount(1, searchTime);
		await UniTask.Delay(System.TimeSpan.FromSeconds(searchTime));
		//deactivate all instantiated filebuttons first and activate if they match the tag
		AddMatches(fileButtons, searchInput);
		AddMatches(keyfileButtons, searchInput);

		//instantiate filebutton if it doesn't exist yet and matches the tag
		for (int i = 0; i < phaseData.phases.Count; i++)
		{
			//Instantiate other file buttons if tag matches
			for (int j = 0; j < phaseData.phases[i].files.Count; j++)
			{
				FileData f = phaseData.phases[i].files[j];
				if (matches.Contains(f))
					continue;
				if (f.tags.ToLower().Contains(search))
				{
					InstantiateFileButton(phaseData.phases[i], phaseData.phases[i].files[j], false);
					matches.Add(f);
					if (!saveData.discoveredFileNames.Contains(f.name))
						saveData.discoveredFileNames.Add(f.name);
					if (!saveData.tags.Contains(search))
					{
						saveData.tags.Add(search);
						InstantiateTagButton(search);
					}
				}
			}
		}
		linearLoaderFill.fillAmount = 0;
		UpdateStatistics();
		searching = false;
	}

	void AddMatches(List<FileButton> filebuttons, string input)
	{
		List<FileData> matchingData = new List<FileData>();
		for (int i = 0; i < filebuttons.Count; i++)
		{
			filebuttons[i].gameObject.SetActive(false);
			if (filebuttons[i].fileData.tags.ToLower().Contains(input.ToLower()))
			{
				if (!filebuttons[i].fileData.isKeyFile || i >= GetCurrentPhase)
					filebuttons[i].gameObject.SetActive(true);
				matchingData.Add(filebuttons[i].fileData);
			}
		}
		foreach (FileData fileData in matchingData)
			matches.Add(fileData);
	}

	void InstantiateFileButton(PhaseData.Phase phase, FileData fileData, bool isKeyFile)
	{
		FileButton fileButton = Instantiate(fileButtonPrefab, fileButtonContainer);
		fileButton.SetFile(phase, fileData);
		if (isKeyFile)
			keyfileButtons.Add(fileButton);
		else
			fileButtons.Add(fileButton);
	}

	void ShowRestoredFiles()
	{
		SetFileTabButtons(repairedButton);
		foreach (FileButton fileButton in fileButtons)
			fileButton.gameObject.SetActive(false);
		for (int i = 0; i < keyfileButtons.Count; i++)
		{
			if (!keyfileButtons[i].Encrypted)
				keyfileButtons[i].gameObject.SetActive(true);
		}
	}

	void ShowResults()
	{
		SetFileTabButtons(allButton);
		for (int i = 0; i < fileButtons.Count; i++)
		{
			fileButtons[i].gameObject.SetActive(false);
			if (matches.Contains(fileButtons[i].fileData))
				fileButtons[i].gameObject.SetActive(true);
		}
		foreach (FileButton keyfileButton in keyfileButtons)
			keyfileButton.gameObject.SetActive(false);
	}

	public void ShowFavFiles()
	{
		SetFileTabButtons(favsButton);
		for (int i = 0; i < fileButtons.Count; i++)
		{
			fileButtons[i].gameObject.SetActive(false);
			if (saveData.favsFileNames.Contains(fileButtons[i].fileData.name))
				fileButtons[i].gameObject.SetActive(true);
		}
		for (int i = 0; i < keyfileButtons.Count; i++)
		{
			keyfileButtons[i].gameObject.SetActive(false);
			if (saveData.favsFileNames.Contains(keyfileButtons[i].fileData.name))
				keyfileButtons[i].gameObject.SetActive(true);
		}
	}

	void InstantiateKeyfileButtons()
	{
		//if (saveData.discoveredFileNames.Count == 0)
		//	return;
		for (int i = 0; i < phaseData.phases.Count; i++)
			InstantiateFileButton(phaseData.phases[i], phaseData.phases[i].keyFile, true);
		foreach (FileButton keyfileButton in keyfileButtons)
			keyfileButton.gameObject.SetActive(false);
	}

	void SetFileTabButtons(Button button)
	{
		allButton.interactable = button != allButton;
		repairedButton.interactable = button != repairedButton;
		favsButton.interactable = button != favsButton;
	}

	public void InstantiateTagButtons()
	{
		for (int i = 0; i < saveData.tags.Count; i++)
			InstantiateTagButton(saveData.tags[i]);
	}

	void InstantiateTagButton(string searchResult)
	{
		TagButton tagButton = Instantiate(tagButtonPrefab, tagButtonContainer);
		tagButton.SetTagButton(searchResult);
	}

	public void UpdateStatistics()
	{
		statisticsText.text = saveData.discoveredFileNames.Count + "\n" +
								DecryptedFilesCount + "\n" +
								CurrentPhase + "\n" +
								saveData.openedFileNames.Count;
		if (saveData.loggedIn)
			loginText.text = DesktopScreen.Instance.IP + "\n" + DesktopScreen.Instance.userName + "\n" + DesktopScreen.Instance.password;
	}

	public async UniTask InstantiateFileWindow(FileButton fileButton)
	{
		WindowBehaviour window = null;
		PhaseData.Phase p = fileButton.phase;
		FileData f = fileButton.fileData;
		if (f.IsPicture)
			window = InstantiateWindow(imageWindowPrefab, fileButton);
		else if(f.IsText)
			window = InstantiateWindow(textWindowPrefab, fileButton);
		else
			window = InstantiateWindow(videoWindowPrefab, fileButton);
		await UniTask.Yield();
		//else if (f.IsPicture)
		//{
		//	window = Instantiate(imageWindowPrefab, transform.parent);
		//	window.ApplyImageFile(f.picture);
		//}
		//else if (f.IsText)
		//{
		//	window = Instantiate(textWindowPrefab, transform.parent);
		//}
		TaskbarButton taskbarButton = Instantiate(taskbarButtonPrefab, DesktopScreen.Instance.taskbarButtonsContainer);
		taskbarButton.SetTaskbarButton(window, fileButton.fileButton.image.sprite);
		window.SetupWindow(f.name, taskbarButton);
		fileButton.window = window;
	}

	public WindowBehaviour InstantiateWindow(WindowBehaviour prefab, FileButton fileButton)
	{
		WindowBehaviour window = Instantiate(prefab, DesktopScreen.Instance.windowContainer);
		window.SetMediaFile(fileButton);
		return window;
	}

	public override void OnWindowOrderChanged(WindowBehaviour window)
	{
		base.OnWindowOrderChanged(window);
		if (window == this)
			Terminal.Instance.TerminalInputField.Select();
		else
			DeselectTerminal();
	}

	async void DeselectTerminal()
	{
		await UniTask.WaitUntil(() => !IsTopWindow);
		Terminal.Instance.TerminalInputField.DeselectTerminal();
	}

	public override void SetWindowState(bool visible, bool minimized)
	{
		base.SetWindowState(visible, minimized);
		if (!visible && !minimized)
			DecrypterLogin.Instance.SetWindowState(false, false);
	}

	[System.Serializable]
	public class SaveData
	{
		public List<string> discoveredFileNames = new List<string>();
		public List<string> openedFileNames = new List<string>();
		public List<string> favsFileNames = new List<string>();
		public List<string> tags = new List<string>();
		public bool loggedIn;
		public string terminalTextSave;
	}
	public SaveData saveData;

}
