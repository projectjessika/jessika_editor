﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using UniRx.Async;
using System.IO;

public class FileButton : MonoBehaviour
{
	public PhaseData.Phase phase { get; private set; }
	public FileData fileData { get; private set; }
	public WindowBehaviour window { get; set; }

	public Button fileButton;
	public Button contextButton;
	public TextMeshProUGUI fileNameText;
	public GameObject thumbnailGo;
	public Image thumbnailImg;

	const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";
	public bool Encrypted //Is Locked
	{
		get
		{
			if (fileData.isKeyFile && phase.index >= DecrypterManager.Instance.CurrentPhase)
				return true;
			if (!fileData.isKeyFile && phase.index > DecrypterManager.Instance.CurrentPhase)
				return true;
			return false;
		}
	}

	public Sprite CurrentPhaseThumbnail
	{
		get
		{
			if (fileData == null)
				return null;
			if (Encrypted)
				return fileData.isKeyFile ? phase.corruptThumbnail : phase.encryptedThumbnail;
			else
			{
				return DecryptedThumbnail;
			}
		}
	}

	public Sprite DecryptedThumbnail
	{
		get
		{
			if (DecrypterManager.Instance.IsFileOpened(fileData.name))
			{
				if (fileData.IsVideo || fileData.IsAudio)
					return phase.mediaThumbnail;
				if (fileData.IsPicture)
					return phase.imageThumbnail;
				if (fileData.IsText)
					return phase.textThumbnail;
				return phase.decryptedThumbnail;
			}
			return phase.decryptedThumbnail;
		}
	}

	Sprite videoThumbnailSprite;
	public Sprite GetVideoThumbnailSprite
	{
		get
		{
			if (GetVideoThumbnailTex2D == null)
				return null;
			if (videoThumbnailSprite == null)
			{
				// Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference
				Texture2D Tex2D = GetVideoThumbnailTex2D;
				videoThumbnailSprite = Sprite.Create(Tex2D, new Rect(0, 0, Tex2D.width, Tex2D.height), new Vector2(0, 0));
			}
			return videoThumbnailSprite;
		}
	}

	Texture2D videoThumbnailTex2D;
	public Texture2D GetVideoThumbnailTex2D
	{
		get
		{
			if (videoThumbnailTex2D == null)
			{
				//Load Texture
				string dir = Application.persistentDataPath + "/Thumbnails/";
				if (!Directory.Exists(dir))
					return null;

				if (fileData.video == null)
					return null;
				string fileName = fileData.video.name + ".png";
				string path = dir + fileName;

				byte[] bytes;

				if (File.Exists(path))
				{
					bytes = File.ReadAllBytes(path);
					videoThumbnailTex2D = new Texture2D(1, 1);
					if (videoThumbnailTex2D.LoadImage(bytes))
						return videoThumbnailTex2D;
				}
				return null;
			}
			return videoThumbnailTex2D;
		}
	}

	private string randomString = null;
	private string GetRandomString
	{
		get
		{
			if (string.IsNullOrEmpty(randomString))
			{
				int charAmount = UnityEngine.Random.Range(5, 16); //set those to the minimum and maximum length of your string
				for (int i = 0; i < charAmount; i++)
					randomString += glyphs[UnityEngine.Random.Range(0, glyphs.Length)];
			}
			return randomString.ToUpper();

		}
	}

	bool IsFav
	{
		get { return DecrypterManager.Instance.IsFavFile(fileData.name); }
	}

	private void Start()
	{
		fileButton.onClick.AddListener(OpenFile);
		contextButton.onClick.AddListener(delegate
		{
			FileContextMenu.Instance.SetWindow(fileData);
		});
	}

	private void OnEnable()
	{
		if (fileData != null)
			UpdateButton();
	}

	public void SetFile(PhaseData.Phase phase, FileData fileData)
	{
		this.phase = phase;
		this.fileData = fileData;
		UpdateButton();
	}

	public void UpdateButton()
	{
		bool isOpened = DecrypterManager.Instance.IsFileOpened(fileData.name);
		bool encrypted = Encrypted;
		contextButton.gameObject.SetActive(!encrypted);
		fileButton.interactable = !encrypted;
		fileButton.image.sprite = CurrentPhaseThumbnail;
		if (fileData.IsVideo && !encrypted && isOpened)
		{
			if (GetVideoThumbnailSprite != null)
			{
				thumbnailImg.sprite = GetVideoThumbnailSprite;
				thumbnailGo.SetActive(true);
			}
			fileButton.targetGraphic = thumbnailImg;
		}
		window?.taskbarButton.SetIcon(CurrentPhaseThumbnail);
		fileNameText.text = isOpened ? fileData.name : GetRandomString;
	}

	public async void OpenFile()
	{
		if (window == null)
			await DecrypterManager.Instance.InstantiateFileWindow(this);
		window.GetComponent<WindowBehaviour>().SetWindowState(true, false);
		DecrypterManager.Instance.AddOpenedFile(fileData.name);
		await UniTask.WaitUntil(() => window != null);
		UpdateButton();
	}

}
