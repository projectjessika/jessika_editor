﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class DesktopScreen : ScreenCanvas
{
	public static DesktopScreen Instance;
	public Transform windowContainer;
	public Transform taskbarButtonsContainer;
	public TextMeshProUGUI systemTimeText;

	public Action<WindowBehaviour> OnWindowOrderChanged;
	public Action<WindowBehaviour, bool> OnOtherMediaPlayed;
	public Action<FileData> OnFavChanged;
	public Action<FileData> OnVideoThumbnailCreated;

	public string playerName;

	public string IP;
	public string userName;
	public string password;


	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		AmbientSounds.Instance.Login();
		GenerateLogin();
		LoadLogin();
	}

	private void Update()
	{
		systemTimeText.text = DateTime.Now.ToShortTimeString() + "\n" + DateTime.Today.ToShortDateString();
	}

	private void OnDestroy()
	{
		AmbientSounds.Instance.Logout();
	}

	private void GenerateLogin()
	{
		SaveLoadManager.SaveGame saveGame = SaveLoadManager.Instance.selectedSaveGame;
		if (saveGame != null)
			return;
		for (int i = 0; i < 4; i++)
		{
			IP += UnityEngine.Random.Range(20, 300).ToString();
			if (i != 3)
				IP += ".";
		}
		userName = "Jessika";
		password = "j3ss1ka_" + UnityEngine.Random.Range(1980, 2020).ToString();
	}

	private void LoadLogin()
	{
		SaveLoadManager.SaveGame saveGame = SaveLoadManager.Instance.selectedSaveGame;
		if (saveGame == null)
			return;
		IP = saveGame.data.IP;
		userName = saveGame.data.username;
		password = saveGame.data.password;
	}
}
