﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ErrorWindow : MonoBehaviour {

	public Text errorText;
	public string textEN;
	public string textDE;

	public void ApplyText(string DE, string EN)
	{
		textEN = EN;
		textDE = DE;
	}

	// Use this for initialization
	void Start () {

		errorText.text = LocalizationManager.CurrentLanguageCode == "de" ? textDE : textEN;
	}
}
