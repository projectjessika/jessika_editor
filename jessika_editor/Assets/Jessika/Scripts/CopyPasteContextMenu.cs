﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Reflection;
using System;

public class CopyPasteContextMenu : ContextWindow
{
	public static CopyPasteContextMenu Instance;
	public Button copyButton;
	public Button pasteButton;

	string storedString = null;

	private void Awake()
	{
		Instance = this;
	}

	public void SetWindow(string stringToCopy = null, TMP_InputField inputField = null, TerminalInputField terminalInputField = null)
	{
		if (stringToCopy != null)
		{
			copyButton.onClick.RemoveAllListeners();
			copyButton.onClick.AddListener(delegate
			{
				Clipboard = stringToCopy;
			});
		}
		if (inputField != null)
		{
			pasteButton.onClick.RemoveAllListeners();
			pasteButton.onClick.AddListener(delegate
			{
				inputField.text = Clipboard;
			});
		}
		if (terminalInputField != null)
		{
			pasteButton.onClick.RemoveAllListeners();
			pasteButton.onClick.AddListener(delegate
			{
				terminalInputField.ActivateInputField();
				terminalInputField.text = Clipboard;
			});
		}
		copyButton.interactable = stringToCopy != null;
		pasteButton.interactable = (inputField != null || terminalInputField != null) && Clipboard != null;
		base.SetWindow();
	}


	internal static string Clipboard
	{
		get
		{
			TextEditor _textEditor = new TextEditor();
			_textEditor.Paste();
			return _textEditor.text;
		}
		set
		{
			TextEditor _textEditor = new TextEditor
			{ text = value };

			_textEditor.OnFocus();
			_textEditor.Copy();
		}
	}
}

