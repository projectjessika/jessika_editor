﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using I2.Loc;
using UniRx.Async;

public class MailManager : WindowBehaviour
{
	public static MailManager instance;
	public List<MailSaveData> mailSaveDatas;

	public GameObject mailButtonPrefab;
	public GameObject mailButtonsContent;
	public TextMeshProUGUI senderText;
	public TextMeshProUGUI subjectText;
	public TextMeshProUGUI dateText;
	public TextMeshProUGUI mailContent;
	public DesktopButton desktopIcon;
	public AudioClip notificationSound;

	public Button inboxButton;
	public TextMeshProUGUI inboxCountText;
	public Button spamButton;
	public TextMeshProUGUI spamCountText;

	List<MailButton> inboxButtons;
	List<MailButton> spamButtons;

	public MailData mailData;

	int GetNewMailCount(List<MailButton> buttons)
	{
		int unread = 0;
		for (int i = 0; i < buttons.Count; i++)
		{
			if (!buttons[i].mailSaveData.opened)
				unread += 1;
		}
		return unread;
	}

	AudioSource _Audio;
	AudioSource Audio
	{
		get { return _Audio = _Audio ?? GetComponent<AudioSource>(); }
	}

	private void Awake()
	{
		instance = this;
	}

	public override void Start()
	{
		base.Start();
		inboxButtons = new List<MailButton>();
		spamButtons = new List<MailButton>();
		InitializeMails();

		spamButton.onClick.AddListener(delegate
		{
			SortButtons(false);
		});
		inboxButton.onClick.AddListener(delegate
		{
			SortButtons(true);
		});
		DecrypterManager.Instance.OnNewFileOpened += TriggerMail;
	}

	public void MailButtonClicked(MailButton mailButton)
	{
		for (int i = 0; i < inboxButtons.Count; i++)
			inboxButtons[i].Button.interactable = inboxButtons[i] != mailButton;
		for (int i = 0; i < spamButtons.Count; i++)
			spamButtons[i].Button.interactable = spamButtons[i] != mailButton;
	}

	async void InitializeMails()
	{
		await UniTask.WaitUntil(() => SaveLoadManager.Instance);
		if (SaveLoadManager.Instance.SaveGameSelected)
		{
			mailSaveDatas = SaveLoadManager.Instance.selectedSaveGame.data.mailSaveData;
			await InstantiateSavedMails();
		}
		else
		{
			for (int i = 0; i < mailData.mails.Count; i++)
			{
				MailData.Mail mail = mailData.mails[i];
				if (mail.trigger == MailData.Trigger.None)
				{
					DateTime dateTime = DateTime.Now.AddDays(UnityEngine.Random.Range(-5, -1));
					InstantiateMail(i, dateTime);
				}
			}
		}
		UpdateCount();
		inboxButton.Select();
		SortButtons(true);
	}


	public void SetOpenMail(string sender, string subject, string date, string content)
	{
		senderText.text = sender;
		subjectText.text = subject;
		dateText.text = date;
		mailContent.SetCharArray(content.ToCharArray());
		mailContent.SetAllDirty();
	}

	public void UpdateCount()
	{
		int inbox = GetNewMailCount(inboxButtons);
		int spam = GetNewMailCount(spamButtons);
		inboxCountText.transform.parent.gameObject.SetActive(inbox > 0);
		spamCountText.transform.parent.gameObject.SetActive(spam > 0);
		inboxCountText.text = inbox.ToString();
		spamCountText.text = spam.ToString();
	}


	public void InstantiateMail(int index, DateTime dateTime)
	{
		MailSaveData mailSaveData = new MailSaveData(index, dateTime, false);
		InstantiateButton(mailSaveData);
		mailSaveDatas.Add(mailSaveData);
		desktopIcon.SetNotification(true);
		Notification();
		UpdateCount();
	}

	public void TriggerMail(int currentPhase, int openedFiles)
	{
		for (int i = 0; i < mailData.mails.Count; i++)
		{
			MailData.Mail mail = mailData.mails[i];

			//Check if mail has already been instantiated, if so, skip this
			MailSaveData mailSaveData = null;
			for (int j = 0; j < mailSaveDatas.Count; j++)
				if (mailSaveDatas[j].index == i)
				{
					mailSaveData = mailSaveDatas[j];
					break;
				}
			if (mailSaveData != null)
				continue;

			//If mail has not already been instantiated and meets the trigger requirements, instantiate!
			if ((int)mail.trigger == currentPhase && mail.triggerRequirementAmount == openedFiles)
				InstantiateMail(i, DateTime.Now);
		}
	}

	private void Notification()
	{
		desktopIcon.SetNotification(true);
		//	Audio.clip = notificationSound;
		Audio.Play();
	}

	private void InstantiateButton(MailSaveData mailSaveData)
	{
		MailButton mailButton = Instantiate(mailButtonPrefab, mailButtonsContent.transform).GetComponent<MailButton>();
		mailButton.mailSaveData = mailSaveData;
		mailButton.transform.SetAsFirstSibling();
		if (!GetMail(mailSaveData).isSpam)
			inboxButtons.Add(mailButton);
		else
			spamButtons.Add(mailButton);
	}

	private void SortButtons(bool isInbox)
	{
		foreach (MailButton button in inboxButtons)
			button.gameObject.SetActive(isInbox);
		foreach (MailButton button in spamButtons)
			button.gameObject.SetActive(!isInbox);
		Canvas.ForceUpdateCanvases();
	}


	async UniTask InstantiateSavedMails()
	{
		if (mailSaveDatas.Count != 0)
		{
			for (int i = 0; i < mailSaveDatas.Count; i++)
			{
				InstantiateButton(mailSaveDatas[i]);
				await UniTask.Yield();
			}
		}
		UpdateCount();
		SortButtons(true);
		inboxButton.Select();
	}


	[System.Serializable]
	public class MailSaveData
	{
		public int index;
		public DateTime dateTimeReceived;
		public bool opened;

		public MailSaveData(int index, DateTime dateTimeReceived, bool opened)
		{
			this.index = index;
			this.dateTimeReceived = dateTimeReceived;
			this.opened = opened;
		}
	}

	public MailData.Mail GetMail(MailSaveData mailSaveData)
	{
		return mailData.mails[mailSaveData.index];
	}
}
