﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Collections;
using TMPro;
using I2.Loc;
using DG.Tweening;
using DG.Tweening.Core;
using System;
using UniRx.Async;
using System.Threading;
using System.IO;

public class MediaPlayerWindow : WindowBehaviour
{
	public Slider slider;
	public Button playButton;
	public Button playButtonScreen;
	public Image playButtonScreenIcon;
	public Sprite playSprite;
	public Sprite pauseSprite;
	public Button translationButton;
	public Button favButton;
	public VideoPlayer player;
	public RawImage videoRawImage;
	public TextMeshProUGUI currentTime;
	public TextMeshProUGUI clipLength;
	public TextMeshProUGUI videoText;
	public RectTransform translationBox;
	public AudioSource audioSrc;
	public GameObject audioVisualizer;

	private CancellationTokenSource _cancellationTokenSource;
	private RenderTexture rt;
	private FileButton fileButton;
	private AudioClip dubDe;
	public FileData data
	{
		get { return fileButton.fileData; }
	}



	CanvasGroup _transBoxCanvasGroup;
	CanvasGroup TransBoxCanvasGroup
	{
		get { return _transBoxCanvasGroup = _transBoxCanvasGroup ?? translationBox.GetComponent<CanvasGroup>(); }
	}

	string GetText
	{
		get { return Settings.Instance.SubtitleLanguage == "en" ? data.videoTranslationEn : data.videoTranslationDe; }
	}

	ContentSizeFitter _contentSizeFitter;
	ContentSizeFitter ContentSizeFitter
	{
		get
		{

			return _contentSizeFitter = _contentSizeFitter ?? translationBox.transform.GetComponent<ContentSizeFitter>();

		}
	}

	string[] Words(string text)
	{
		char[] delimiters = new char[] { ' ', '\r', '\n' };
		return text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
	}

	int currentWordCount
	{
		get { return (int)Math.Floor((Words(GetText).Length * 1.5f) / ClipLength * CurrentTime); }
	}

	//returns the current time of the clip
	public float CurrentTimeDisplay
	{
		get { return player.isActiveAndEnabled ? slider.value * ClipLength : 0; }
	}

	public float CurrentTime
	{
		get { return data.IsVideo ? (float)player.time : (float)audioSrc.time; }
	}

	public float ClipLength
	{
		get { return data.IsVideo ? (float)data.video.length : data.audio.length; }
	}

	//Transforms a time into a readable string eg. 05:45
	string TimeDisplay(float time)
	{
		return player.isActiveAndEnabled ? string.Format("{0:00}:{1:00}", Mathf.Floor(time / 60), time % 60) : string.Empty;
	}

	public bool IsFinished
	{
		get { return player.time == data.video.length; }
	}

	public override void Start()
	{
		base.Start();
		OnWindowResize += MoveTranslationBox;
		Settings.Instance.OnAudioChange += SetAudioLanguage;
		Settings.Instance.OnSubtitleChange += SetSubtitleLanguage;
		translationButton.onClick.AddListener(delegate
		{
			translationButton.image.color = TransBoxCanvasGroup.alpha == 0 ? translationButton.colors.highlightedColor : Color.white;
			SetTranslationBox(TransBoxCanvasGroup.alpha == 0);
		});
		playButton.onClick.AddListener(OnPlayButton);
		playButtonScreen.onClick.AddListener(delegate
		{
			OnPlayButton();
			TweenScreenButton();
		});

	}

	public override void OnEnable()
	{
		base.OnEnable();
		DesktopScreen.Instance.OnWindowOrderChanged += HideTranslationBox;
		DesktopScreen.Instance.OnOtherMediaPlayed += OtherMediaPlayed;
		DesktopScreen.Instance.OnFavChanged += FavChanged;
		LiveTranslate();
	}

	public override void OnDisable()
	{
		base.OnDisable();
		DesktopScreen.Instance.OnWindowOrderChanged -= HideTranslationBox;
		DesktopScreen.Instance.OnOtherMediaPlayed -= OtherMediaPlayed;
		DesktopScreen.Instance.OnFavChanged -= FavChanged;
		_cancellationTokenSource.Cancel();
	}

	private void FixedUpdate()
	{
		if (data != null)
			currentTime.text = TimeDisplay(CurrentTimeDisplay);
	}

	public override void SetWindowState(bool visible, bool minimized)
	{
		base.SetWindowState(visible, minimized);
		if (!visible && !minimized)
			Stop();
	}

	async void CreateThumbnail()
	{
		if (fileButton.GetVideoThumbnailSprite != null)
			return;
		await UniTask.WaitUntil(() => player.isPlaying);
		await UniTask.Delay(TimeSpan.FromSeconds(1f));

		string pngName = data.video.name + ".png";
		string dir = Application.persistentDataPath + "/Thumbnails/";
		if (!Directory.Exists(dir))
			Directory.CreateDirectory(dir);

		Texture2D tex2D = ToTexture2D(rt);
		byte[] bytes = tex2D.EncodeToPNG();
		File.WriteAllBytes(dir + pngName, bytes);
		data.thumbnail = Sprite.Create(tex2D, new Rect(0, 0, tex2D.width, tex2D.height), new Vector2(0, 0));
		Debug.Log("Created thumbnail: " + dir + pngName);
		fileButton.UpdateButton();
	}

	Texture2D ToTexture2D(RenderTexture rTex)
	{
		Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGBA32, false);
		RenderTexture.active = rTex;
		tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
		tex.Apply();
		return tex;
	}

	public async void LiveTranslate()
	{
		if (fileButton == null || data == null || videoText.text == GetText)
			return;
		try
		{
			_cancellationTokenSource = new CancellationTokenSource();
			if (data.IsAudio)
			{
				await UniTask.WaitUntil(() => audioSrc.clip != null);
				await UniTask.WaitUntil(() => audioSrc.isPlaying, cancellationToken: _cancellationTokenSource.Token);
			}
			else if (data.IsVideo)
			{
				await UniTask.WaitUntil(() => player.clip != null);
				if (_cancellationTokenSource != null)
					await UniTask.WaitUntil(() => player.isPlaying, cancellationToken: _cancellationTokenSource.Token);
			}
			int wordCount = Words(videoText.text).Length;
			if (wordCount < Words(GetText).Length)
			{
				for (int i = wordCount; i < currentWordCount; i++) //let word appear
				{
					if (i > Words(GetText).Length - 1)
						break;
					string word = Words(GetText)[i];
					char[] letters = word.ToCharArray();
					for (int j = 0; j < letters.Length; j++) //let letters of word appear
					{
						await UniTask.Delay(TimeSpan.FromSeconds(0.1f));
						videoText.text += letters[j];
						if (j == letters.Length - 1)
							videoText.text += " ";
					}
					UpdateCanvas();
				}
			}
			else
				videoText.text = GetText;
			LiveTranslate();
		}
		catch (OperationCanceledException e)
		{
			Debug.Log(e.Message);
		}
	}

	void UpdateCanvas()
	{
		ContentSizeFitter.enabled = false;
		Canvas.ForceUpdateCanvases();
		ContentSizeFitter.enabled = true;
	}


	void OnPlayButton()
	{
		DesktopScreen.Instance.OnOtherMediaPlayed?.Invoke(this, false);
		if (data.IsVideo && player.isPlaying)
			Pause();
		else if (data.IsAudio && audioSrc.isPlaying)
			Pause();
		else
			Play();
	}

	public void OtherMediaPlayed(WindowBehaviour window, bool pauseSelf)
	{
		if (window == this && !pauseSelf)
			return;
		Pause();
	}

	public void Play()
	{
		playButton.image.sprite = pauseSprite;
		playButtonScreenIcon.sprite = playSprite;
		if (data.IsVideo)
		{
			player.Play();
			if (playButtonScreen.image.color.a != 0)
				playButtonScreen.image.DOFade(0, 0.3f);
			CreateThumbnail();
		}
		audioVisualizer.SetActive(data.IsAudio);
		audioSrc.Play();
	}

	public void Pause()
	{
		playButton.image.sprite = playSprite;
		playButtonScreenIcon.sprite = pauseSprite;
		if (data.IsVideo)
			player.Pause();
		audioSrc.Pause();
	}

	public void Stop()
	{
		playButton.image.sprite = playSprite;
		playButtonScreenIcon.sprite = playSprite;
		if (data.IsVideo)
			player.Stop();
		audioVisualizer.SetActive(false);
		audioSrc.Stop();
		slider.value = 0;
	}

	void TweenScreenButton()
	{
		playButtonScreenIcon.DOKill();
		playButtonScreenIcon.DOFade(0.7f, 0.3f).OnComplete(() => playButtonScreenIcon.DOFade(0f, 0.3f)); //Fade image in and out
		playButtonScreenIcon.rectTransform.DOSizeDelta(playButtonScreenIcon.rectTransform.sizeDelta + (playButtonScreenIcon.rectTransform.sizeDelta * 0.2f), 0.3f).
		OnComplete(() => playButtonScreenIcon.rectTransform.DOSizeDelta(playButtonScreenIcon.rectTransform.sizeDelta - (playButtonScreenIcon.rectTransform.sizeDelta * 0.2f), 0.3f));
	}

	public override void SetMediaFile(FileButton fileButton)
	{
		//Instantiate Rendertexture and Material
		Debug.Log(fileButton.fileData.name);
		this.fileButton = fileButton;
		if (data.IsVideo)
		{
			rt = new RenderTexture(524, 300, 16, RenderTextureFormat.ARGB32);
			rt.Create();
			Material videoMat = new Material(Shader.Find("Sprites/Default"));
			videoRawImage.texture = rt;
			videoRawImage.material = videoMat;

			//fill the player on first start up
			player.targetTexture = rt;
			player.clip = data.video;
			player.enabled = false;
			player.enabled = true;
			dubDe = audioSrc.clip;
		}
		else if (data.IsAudio)
		{
			player.enabled = false;
			videoRawImage.enabled = false;
		}
		SetAudioLanguage();
		clipLength.text = TimeDisplay(ClipLength);


		SetFavButtonColor();
		favButton.onClick.AddListener(delegate
		{
			DecrypterManager.Instance.AddRemoveFavFile(data);
		});
		LiveTranslate();

		if (data.isKeyFile && fileButton.Encrypted)
			WaitForKeyfile();
	}

	void SetAudioLanguage()
	{
		Stop();
		if (data.IsVideo)
		{
			player.audioOutputMode = Settings.Instance.AudioLanguage == "en" ? VideoAudioOutputMode.None : VideoAudioOutputMode.AudioSource;
			if (player.audioOutputMode == VideoAudioOutputMode.AudioSource)
				player.SetTargetAudioSource(0, audioSrc);
			audioSrc.clip = Settings.Instance.AudioLanguage == "en" ? data.dubEn : dubDe;
		}
		else
			audioSrc.clip = Settings.Instance.AudioLanguage == "en" ? data.dubEn : data.audio;
	}

	void SetSubtitleLanguage()
	{
		Stop();
		_cancellationTokenSource.Cancel();
		videoText.text = "";
		LiveTranslate();
	}

	async void WaitForKeyfile()
	{
		Play();
		TweenScreenButton();
		translationButton.image.color = TransBoxCanvasGroup.alpha == 0 ? translationButton.colors.highlightedColor : Color.white;
		SetTranslationBox(TransBoxCanvasGroup.alpha == 0);
		MouseBehaviour.Instance.SetRadialLoader(true);
		DesktopScreen.Instance.CanvasGroup.blocksRaycasts = false;
		if (data.IsVideo)
			await UniTask.WaitUntil(() => player.time == data.video.length);
		else
			await UniTask.WaitUntil(() => audioSrc.time == data.audio.length);
		DesktopScreen.Instance.CanvasGroup.blocksRaycasts = true;
		MouseBehaviour.Instance.SetRadialLoader(false);
	}

	void SetFavButtonColor()
	{
		Color currentColor = DecrypterManager.Instance.IsFavFile(data.name) ? new Color(1, 0.8f, 0, 1f) : new Color(0, 0, 0, 0.2f);
		ColorBlock cb = favButton.colors;
		cb.normalColor = currentColor;
		favButton.colors = cb;
	}

	void FavChanged(FileData fileData)
	{
		if (fileData == this.data)
			SetFavButtonColor();
	}

	public async void SetTime(float time, bool play)
	{
		if (play)
			Play();
		if (data.IsVideo)
		{
			player.time = time;
			await UniTask.WaitUntil(() => player.isPlaying);
		}
		audioSrc.time = time;
	}

	void MoveTranslationBox()
	{
		Vector2 anchor = IsMaximized ? new Vector2(1, 0.5f) : new Vector2(1, 0);
		translationBox.DOAnchorMin(anchor, tweenTime);
		translationBox.DOAnchorMax(anchor, tweenTime);
		translationBox.DOPivotX(IsMaximized ? 1 : 0, tweenTime);
		translationBox.DOPivotY(IsMaximized ? 0.5f : 0, tweenTime);
		translationBox.DOAnchorPos(IsMaximized ? new Vector2(-20, 0) : new Vector2(20, 0), tweenTime);
	}

	void SetTranslationBox(bool visible)
	{
		TransBoxCanvasGroup.DOKill();
		TransBoxCanvasGroup.interactable = visible;
		TransBoxCanvasGroup.blocksRaycasts = visible;
		TransBoxCanvasGroup.DOFade(visible ? 1 : 0, tweenTime);
	}

	void HideTranslationBox(WindowBehaviour window)
	{
		if (window == this)
			return;
		SetTranslationBox(false);
		translationButton.image.color = Color.white;
	}

	public override void OnWindowOrderChanged(WindowBehaviour window) => base.OnWindowOrderChanged(window);

}
