﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

	public class SubMenus
	{
		public GameObject mainMenu;
		public GameObject SaveMenu;
	}
	public SubMenus subMenus;

	public void ShowSaveMenu()
	{
		subMenus.SaveMenu.SetActive(true);
	}

	private void DisableOtherMenus(GameObject enabledGO)
	{
		
	}
}
