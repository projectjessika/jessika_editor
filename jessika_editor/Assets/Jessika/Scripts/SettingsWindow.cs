﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SettingsWindow : WindowBehaviour
{

	SaveLoadManager _SaveLoad;
	SaveLoadManager SaveLoad
	{
		get { return _SaveLoad = _SaveLoad ?? FindObjectOfType<SaveLoadManager>(); }
	}

	public TextMeshProUGUI playerName;
	public Button Save;
	public Button Logout;

	// Use this for initialization
	public override void Start()
	{
		base.Start();
		Save.onClick.AddListener(delegate
		{
			SaveLoad.Save();
		});
	}


	public override void SetWindowState(bool visible, bool _minimized)
	{
		float val = visible ? 1 : 0;
		if (visible)
		{
			transform.localScale = Vector3.zero;
			gameObject.SetActive(true);
			LeanTween.scale(gameObject, new Vector3(val, val, val), .2f);
		}
		else
			LeanTween.scale(gameObject, new Vector3(val, val, val), .2f).setOnComplete(DisableWindow);
	}

	void DisableWindow()
	{
		gameObject.SetActive(false);
	}

}
