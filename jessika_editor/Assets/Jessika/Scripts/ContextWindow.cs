﻿using UnityEngine;
using DG.Tweening;

public class ContextWindow : MonoBehaviour
{
	CanvasGroup _canvasGroup;
	CanvasGroup CanvasGroup
	{
		get { return _canvasGroup = _canvasGroup ?? GetComponent<CanvasGroup>(); }
	}

	public bool Visible
	{
		get { return CanvasGroup.alpha == 1; }
	}

	protected virtual void Update()
	{
		if (Input.GetMouseButtonDown(0) && Visible)
			SetWindowState(false);
	}

	public virtual void SetWindow()
	{
		transform.position = MouseBehaviour.Instance.mouse.transform.position;
		SetWindowState(true);
	}


	private void SetWindowState(bool visible)
	{
		CanvasGroup.DOFade(visible ? 1 : 0, 0.2f).OnComplete(() =>
		{
			CanvasGroup.interactable = visible;
			CanvasGroup.blocksRaycasts = visible;
		});
		if (visible)
			transform.SetAsLastSibling();
	}
}
