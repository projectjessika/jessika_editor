﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class SaveGameButton : MonoBehaviour
{
    public Button confirmDeleteButton;
    public TextMeshProUGUI userNameText;
    public TextMeshProUGUI dateText;
    public TextMeshProUGUI timeText;

    Button _button;
    Button button
    { get { return _button = _button ?? GetComponent<Button>(); } }

    private SaveLoadManager.SaveGame saveGame;

    public void SetSaveGameButton(SaveLoadManager.SaveGame saveGame)
    {
        button.onClick.RemoveAllListeners();
        confirmDeleteButton.onClick.RemoveAllListeners();

        this.saveGame = saveGame;
        userNameText.text = saveGame.name.Replace(".sav", ""); // only show name and not complete filename
        dateText.text = saveGame.lastAccessedDateTime.ToShortDateString();
        timeText.text = saveGame.lastAccessedDateTime.ToShortTimeString();

        button.onClick.AddListener(delegate
        {
            SaveLoadManager.Instance.selectedSaveGame = saveGame;
            UICanvasController.Instance.ShowDesktopScreen(saveGame.GetPlayerName);
            Settings.Instance.LoadLanguages(saveGame);
        });

        confirmDeleteButton.onClick.AddListener(delegate
        {
            SaveLoadManager.Instance.Delete(this.saveGame);
            gameObject.SetActive(false);
        });
    }

}
