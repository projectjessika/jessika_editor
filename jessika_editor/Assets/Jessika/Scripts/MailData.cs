﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;
[CreateAssetMenu(fileName = "MailData", menuName = "New MailData")]
public class MailData : ScriptableObject
{
	public List<Mail> mails;
	public enum ContentType { text, picture};
	public enum Trigger { Phase_0, Phase_1, Phase_2, Phase_3, Phase_4, None }

	[Serializable]
	public class Mail
	{
		public string correspondent;
		public string subjectEN;
		public string subjectDE;
		public bool isSpam;
		public Trigger trigger;
		public int triggerRequirementAmount;
		public List<Content> contents;

		public string GetSubject
		{
			get { return LocalizationManager.CurrentLanguageCode == "en" ? subjectEN : subjectDE; }
		}
	}
	[Serializable]
	public class Content
	{
		public ContentType contentType;
		public string textEN;
		public string textDE;
		public Sprite picture;

		public string GetText
		{
			get { return LocalizationManager.CurrentLanguageCode == "en" ? textEN : textDE; }
		}
	}
}
